
#include "AlgoAnalyst.h"


AlgoAnalyst::AlgoAnalyst() {
    g = new Graph();
}

AlgoAnalyst::~AlgoAnalyst() {
    delete g;
}

Algorithm* AlgoAnalyst::generateAlgorithm(AlgoAnalyst::algoType type) {

    if(type == AlgoAnalyst::HAMILTONIANCIRCUIT)
        return new HamiltonianCircuit(g);
    else if(type == AlgoAnalyst::GENETICALGORITHM)
        return new GeneticAlgorithm(g);
    else if(type == AlgoAnalyst::TABUSEARCH)
        return new TabuSearch(g);
    else if(type == AlgoAnalyst::PARTICLESWARM)
        return new ParticleSwarm(g);
    else if (type == AlgoAnalyst::SA)
        return new SimulatedAnnealing(g);
    else
        std::cout << "Error generating" << std::endl;
        exit(EXIT_FAILURE);

}

void AlgoAnalyst::load() {
    g->adjList = FileHandler::parse();


}

void AlgoAnalyst::output() {}