
#ifndef LAB_5_PARTICLESWARM_H
#define LAB_5_PARTICLESWARM_H

#include "Algorithm.h"
#include "Graph.h"
#include "Particle.h"
#include <vector>
#include <string>
#include <math.h>
#include <sstream>
#include <random>
#include <stdexcept>
#include <chrono>
#include <iostream>
#include <algorithm>

#define INF 0x3f3f3f3f

inline std::vector<int> operator+(std::vector<int> lhs, std::vector<int> rhs){
    if(lhs.size() != rhs.size())
        throw std::runtime_error("Vectors must be the same size to add");

    std::vector<int> result;

    //loop through each index and add the result of the addition of lhs[i] and rhs[i] to result
    for(int i = 0; i < lhs.size(); i++)
        result.push_back(lhs.at(i) + rhs.at(i));

    return result;

}

inline std::vector<float> operator+(std::vector<float> lhs, std::vector<float> rhs){
    if(lhs.size() != rhs.size())
        throw std::runtime_error("Vectors must be the same size to add");

    std::vector<float> result;

    //loop through each index and add the result of the addition of lhs[i] and rhs[i] to result
    for(int i = 0; i < lhs.size(); i++)
        result.push_back(lhs.at(i) + rhs.at(i));

    return result;

}

inline std::vector<float> operator-(std::vector<float> lhs, std::vector<float> rhs){
    if(lhs.size() != rhs.size())
        throw std::runtime_error("Vectors must be the same size to subtract");

    std::vector<float> result;

    //loop through each index and subtract the result of the addition of lhs[i] and rhs[i] to result
    for(int i = 0; i < lhs.size(); i++)
        result.push_back(lhs.at(i) - rhs.at(i));

    return result;
}

inline std::vector<float> operator*(float val, std::vector<float> vec){
    std::vector<float> result;

    for(float num : vec){
        result.push_back((num * val));
    }

    return result;
}

class ParticleSwarm : public Algorithm{

//public attribute definitions
public:
    typedef std::pair<float,std::vector<int>> solution_t;
    typedef std::vector<Particle> swarm_t;
    typedef std::chrono::high_resolution_clock clock;

//private attribute definitions
private:

    //array of correct solutions found from dynamic tsp
    std::string dynamicSolutions[12] = {"","","","","1 5 2 3 4 1","1 5 2 3 4 6 1","1 6 4 3 2 5 7 1","1 6 4 3 2 8 5 7 1","1 6 9 4 3 2 8 5 7 1","1 6 9 4 10 3 2 8 5 7 1","1 6 9 4 11 10 3 2 8 5 7 1","1 6 9 4 11 10 3 12 2 8 5 7 1"};

    //current dynamic solution
    std::string dynamicSolution;
    std::string reversedSolution;

    std::vector<int> nodes;

    Graph* searchGraph;

    std::pair<float,std::string> solution;

    int totalIterations = 0;

    //caps the velocity to a maximum of VMAX
    int VMAX;

    //c1 and c2 are the learning rates used in velocity calculation
    int c1,c2;

    //high_resolution_clock instance used to store the total execution time
    std::chrono::microseconds executionTime;

    //dynamic execution time
    std::chrono::duration<int,std::micro> dynamicTime;


//public function definitions
public:

    ParticleSwarm();
    ParticleSwarm(Graph*);
    ~ParticleSwarm();
    void load();
    void select(Algorithm::algoType);
    void select(int,int,int);
    void execute();
    void output();
    void display();



private:

    //particle swarm functions
    void calculateVelocity(swarm_t&,std::vector<int> gBest);
    void calculateParticleVelocity(Particle&, std::vector<int> );
    void updatePosition(swarm_t&);
    void updateParticleBest(swarm_t&);
    std::pair<float,std::string> particle_swarm();


    //vector math utilities
    friend std::vector<int> operator+(std::vector<int>,std::vector<int>);
    friend std::vector<float> operator+(std::vector<float>,std::vector<float>);
    friend std::vector<float> operator-(std::vector<float>,std::vector<float>);
    friend std::vector<float> operator*(float,std::vector<float>);
    float vector_magnitude(std::vector<float>);
    std::vector<int> normalize(std::vector<float>);

    //utility functions
    swarm_t initializeSwarm();
    Particle calculateFitness(swarm_t&);
    float calculateDistance(std::tuple<float, float, float>, std::tuple<float, float, float>);


};





#endif //LAB_5_PARTICLESWARM_H
