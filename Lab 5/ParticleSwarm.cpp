
#include "ParticleSwarm.h"


ParticleSwarm::ParticleSwarm() {}

ParticleSwarm::ParticleSwarm(Graph * g) {

    searchGraph = g;

    dynamicSolution = dynamicSolutions[searchGraph->size() - 1];
    reversedSolution = dynamicSolution;
    std::reverse(reversedSolution.begin(),reversedSolution.end());

    //add each node to nodes vector
    for(auto n : g->adjList){
        if(n.first != 1)
            nodes.push_back(n.first);
    }

    std::chrono::duration<int,std::micro> time(71739785);
    dynamicTime = time;

}

ParticleSwarm::~ParticleSwarm() {}

void ParticleSwarm::load() {}

void ParticleSwarm::select(Algorithm::algoType) {}

void ParticleSwarm::select(int rate1,int rate2, int max) {
    c1 = rate1;
    c2 = rate2;
    VMAX = max;
}

void ParticleSwarm::execute() {
    auto start = clock::now();
    solution = particle_swarm();
    auto end = clock::now();
    executionTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
}

void ParticleSwarm::output() {}

void ParticleSwarm::display() {
    std::cout << "\n\nParticle Swarm\n"
              << "Learning Rates: " << c1 << " and " << c2 << "\n"
              << "VMAX: " << VMAX << "\n"
              << "Solution Path: " << solution.second << "\n"
              << "Solution Cost: " << solution.first << "\n"
              << "Iterations Surpassed: " << totalIterations << "\n"
              << "Execution Time: " << executionTime.count() << " microseconds" << std::endl;
}


//Particle Swarm functions

void ParticleSwarm::calculateParticleVelocity(Particle & particle, std::vector<int> gBest) {
    //using random library from c++11
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_real_distribution<float> probability_range(0.0,1.0);

    //declaring vectors of type float for the calculations
    std::vector<float> initialVel(particle.velocity.begin(),particle.velocity.end()),
                       initialPos(particle.position.begin(),particle.position.end()),
                       pBest(particle.pBest.second.begin(),particle.pBest.second.end()),
                       gBestf(gBest.begin(),gBest.end());

    float rand1 = probability_range(generator),rand2 = probability_range(generator);

    //Velocity formula:    Vf = V0 + c1*rand(Xpbest - X0) + c1*rand(Xgbest - X0)

    std::vector<int> finalVelocity = particle.velocity + normalize( ((c1 * rand1) * (pBest - initialPos)) + ((c2 * rand2) * (gBestf - initialPos)) );


    particle.velocity = finalVelocity;



}

void ParticleSwarm::calculateVelocity(ParticleSwarm::swarm_t & swarm, std::vector<int> gBest) {
    //calculate the velocity for each particle in the swarm
    for(auto& particle : swarm){
        calculateParticleVelocity(particle, gBest);
    }
}

void ParticleSwarm::updatePosition(swarm_t & swarm) {
    //update the position for each particle by adding the updated velocity to its position.......Xf = X0 + V
    for(auto& particle : swarm){

        std::vector<int> nonZeroIndexes, nonZeros;
        //record all indexes where the change in velocity is non-zero
        for(int j = 0; j < particle.velocity.size();j++){
            if(particle.velocity[j] != 0){
                nonZeroIndexes.push_back(j);
                nonZeros.push_back(particle.position[j]);
            }
        }

        random_shuffle(nonZeros.begin(),nonZeros.end());

        //re-insert the values in order of greatest magnitude
        for(int k = 0; k < nonZeroIndexes.size();k++)
            particle.position[nonZeroIndexes[k]] = nonZeros[k];


    }
}

void ParticleSwarm::updateParticleBest(ParticleSwarm::swarm_t & swarm) {
    std::vector<int> placeHolder(swarm[0].velocity.size(),0);

    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> index_range(0,(int)(swarm[0].position.size()) - 1);

    for(auto& particle : swarm){
        swarm_t* localNeighborhood = new swarm_t();
        Particle localBest;

        localNeighborhood->push_back(particle);
        //generate a local neighborhood for each particle
        for(int i = 0; i < particle.position.size();i++){
            std::vector<int> tempVec = particle.position;

            //swap tempVec[i] with a random index in the range
            int index = index_range(generator);
            int temp = tempVec[i];
            tempVec[i] = tempVec[index];
            tempVec[index] = temp;

            localNeighborhood->push_back(Particle(0.0,placeHolder,tempVec,std::make_pair(0.0,placeHolder)));
        }

        localBest =  calculateFitness(*localNeighborhood);
        delete localNeighborhood;
        particle.pBest = std::make_pair(localBest.fitness,localBest.position);
    }
}

std::pair<float,std::string> ParticleSwarm::particle_swarm() {
    int iterations = 0;
    swarm_t swarm;
    Particle gBest,localBest;
    std::string bestPath = "";

    auto start = clock::now();

    //initialize swarm
    swarm = initializeSwarm();
    gBest = calculateFitness(swarm);

    std::stringstream ss;
    copy(gBest.position.begin(),gBest.position.end(),std::ostream_iterator<int>(ss," "));
    bestPath = "";
    bestPath += "1 " +  ss.str() + "1";

    do{
        if(bestPath == dynamicSolution || bestPath == reversedSolution){
            totalIterations = iterations;
            return std::make_pair(gBest.fitness,bestPath);
        }
        iterations++;

        updateParticleBest(swarm);
        calculateVelocity(swarm,gBest.position);
        updatePosition(swarm);

        //recalculate fitness values and set global best
        localBest = calculateFitness(swarm);

        //if localBest is better than global best, update global best
        if(localBest.fitness < gBest.fitness){
            gBest = localBest;
            std::stringstream tempSS;
            ss.swap(tempSS);
            copy(gBest.position.begin(),gBest.position.end(),std::ostream_iterator<int>(ss," "));
            bestPath = "";
            bestPath += "1 " +  ss.str() + "1";
            auto currTime = clock::now();
            std::cout << bestPath << " Cost: " << gBest.fitness << " " << std::chrono::duration_cast<std::chrono::microseconds>(currTime - start).count()  << std::endl;
        }



    }
    while (iterations < 1000);

    totalIterations = iterations;

    std::stringstream tempSS;
    ss.swap(tempSS);
    copy(gBest.position.begin(),gBest.position.end(),std::ostream_iterator<int>(ss," "));
    bestPath = "";
    bestPath += "1 " +  ss.str() + "1";

    return std::make_pair(gBest.fitness,bestPath);

}





//calculates the magnitude of a given vector
float ParticleSwarm::vector_magnitude(std::vector<float> vec) {
    float result = 0.0;
    float sumSquares = 0;

    //add up the squares of each value
    for(float val : vec)
        sumSquares += (val * val);

    result = sqrt(sumSquares);

    return result;

}

//normalizes the vector
std::vector<int> ParticleSwarm::normalize(std::vector<float> vec) {
    float magnitude = vector_magnitude(vec);

    std::vector<int> result;

    for(int i = 0; i < vec.size(); i ++){
        float norm = (vec[i] * VMAX / magnitude);
        result.push_back(ceil(norm));
    }

    return result;
}

//Utility Functions
ParticleSwarm::swarm_t ParticleSwarm::initializeSwarm() {
    swarm_t initSwarm;
    std::vector<int> tempSolution;
    std::vector<int> tempVelocity(nodes.size(),0);

    //initial population size is the number of nodes * 10
    for(int i = 0; i < (nodes.size() * 10); i++){
        if(i == 0){
            //if first iteration, push back particle with position of original permutation
            initSwarm.push_back(Particle(0.0,tempVelocity,nodes,std::make_pair(0.0,nodes)));
        }
        else{
            //push back a particle with a random permutation as the position
            tempSolution = nodes;
            random_shuffle(tempSolution.begin(),tempSolution.end());
            initSwarm.push_back(Particle(0.0,tempVelocity,tempSolution,std::make_pair(0.0,tempSolution)));
        }

    }

    return initSwarm;

}

Particle ParticleSwarm::calculateFitness(ParticleSwarm::swarm_t &swarm) {
   // solution_t best = std::make_pair(INF,std::vector<int>(1));
    Particle best;
    best.fitness = INF;


    for(auto& part: swarm){
        float fitness = 0.0;
        //calculate distance from start node to second node
        fitness += calculateDistance(searchGraph->adjList[1],searchGraph->adjList[part.position[0]]);
        //for each node.. calculate distance from that node to its adjacent node
        for(int i = 0; i < part.position.size() - 1;i++){
            std:: tuple<float,float,float> n1 = searchGraph->operator[](part.position[i]), n2 = searchGraph->operator[](part.position[i + 1]);
            fitness += calculateDistance(n1,n2);

        }
        //calculate distance from last node back to start node
        fitness += calculateDistance(searchGraph->operator[](part.position[part.position.size() - 1]),searchGraph->operator[](1));
        part.fitness = fitness;

        //if current chromosome's fitness is better than current best's then replace best with the current
        if(fitness < best.fitness){
            best = part;
        }

    }

    return best;


}

float ParticleSwarm::calculateDistance(std::tuple<float, float, float> n1, std::tuple<float, float, float> n2) {
    float distance = 0.0;
    float x1 = std::get<0>((n1)) , y1 = std::get<1>((n1)) , z1 = std::get<2>((n1));
    float x2 = std::get<0>((n2)) , y2 = std::get<1>((n2)) , z2 = std::get<2>((n2));

    //use distance formula to calculate distance
    distance  += sqrt((pow((x1 - x2),2.0)) + (pow((y1 - y2),2.0)) + (pow((z1 - z2),2.0)));

    return distance;
}
