

#ifndef LAB_5_SIMULATEDANNEALING_H
#define LAB_5_SIMULATEDANNEALING_H


#include "Algorithm.h"
#include "Graph.h"
#include "SimulatedAOperators.h"
#include <vector>
#include <chrono>
#include <string>
#include <sstream>
#include <iostream>
#include <map>
#include <random>

#define INF 0x3f3f3f3f

class SimulatedAnnealing : public Algorithm {

public:
    typedef std::pair<float,std::vector<int>> solution_t;
    typedef std::vector<solution_t> neighborhood_t;
    typedef std::chrono::high_resolution_clock clock;

    enum coolingTypes {
        EXPONENTIAL = 0,
        LINEAR
    };

    enum coolingLocation {
        EVERYWORSE = 0,
        EVERYITERATION
    };

    const std::string coolingTypeStrings[2] = {"Exponential Decay Cooling", "Linear Cooling"};
    const std::string coolingLocationStrings[2] = {"Every Worse Move", "Every Iteration"};

private:
    //array of correct solutions found from dynamic tsp
    std::string dynamicSolutions[12] = {"","","","","1 5 2 3 4 1","1 5 2 3 4 6 1","1 6 4 3 2 5 7 1","1 6 4 3 2 8 5 7 1","1 6 9 4 3 2 8 5 7 1","1 6 9 4 10 3 2 8 5 7 1","1 6 9 4 11 10 3 2 8 5 7 1","1 6 9 4 11 10 3 12 2 8 5 7 1"};

    //current dynamic solution
    std::string dynamicSolution;
    std::string reversedSolution;

    std::map<SimulatedAnnealing::coolingTypes ,void(*)(int,int&,int,int)> coolingTypeFunctions;

    void(*activeCoolingType)(int,int&,int,int);

    int activeCoolingLocation;

    std::string coolTypeStr;
    std::string coolLocationStr;

    std::vector<int> nodes;

    Graph* searchGraph;

    std::pair<float,std::string> solution;

    int totalIterations = 0;
    int nodeCount;
    int initialTemperature;
    //high_resolution_clock instance used to store the total execution time
    std::chrono::microseconds executionTime;


public:
    SimulatedAnnealing();
    SimulatedAnnealing(Graph*);
    void load();
    void select(Algorithm::algoType);
    void select(coolingTypes,coolingLocation,int);
    void execute();
    void output();
    void display();


private:

    void calculateFitness(neighborhood_t&);
    void calculateSolutionFitness(solution_t&);
    float calculateDistance(std::tuple<float, float, float> n1, std::tuple<float, float, float> n2);
    std::pair<float,std::string> simulated_annealing();

    neighborhood_t generateNeighborhood(solution_t);
    solution_t generateInitialSolution();
    solution_t selectNeighbor(solution_t,neighborhood_t,int&,int);


};


#endif //LAB_5_SIMULATEDANNEALING_H
