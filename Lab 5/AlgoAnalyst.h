
#ifndef LAB_3_ALGOANALYST_H
#define LAB_3_ALGOANALYST_H



#include "HamiltonianCircuit.h"
#include "GeneticAlgorithm.h"
#include "TabuSearch.h"
#include "ParticleSwarm.h"
#include "SimulatedAnnealing.h"
#include "Graph.h"
#include <fstream>
#include <sstream>
#include "FileHandler.h"

//main interface with a factory built in
class AlgoAnalyst {

public:

     enum algoType {
        HAMILTONIANCIRCUIT = 0,
        GENETICALGORITHM,
        TABUSEARCH,
        PARTICLESWARM,
        SA

    };

    AlgoAnalyst();
    ~AlgoAnalyst();
    void load();
    void output();
    Algorithm* generateAlgorithm(AlgoAnalyst::algoType);

private:

    Graph* g;


};


#endif //LAB_3_ALGOANALYST_H
