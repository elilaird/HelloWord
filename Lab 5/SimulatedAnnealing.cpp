

#include "SimulatedAnnealing.h"


SimulatedAnnealing::SimulatedAnnealing() {}

SimulatedAnnealing::SimulatedAnnealing(Graph * g) {
    searchGraph = g;

    dynamicSolution = dynamicSolutions[searchGraph->size() - 1];
    reversedSolution = dynamicSolution;
    std::reverse(reversedSolution.begin(),reversedSolution.end());

    //add each node to nodes vector
    for(auto n : g->adjList){
        if(n.first != 1)
            nodes.push_back(n.first);
    }

    nodeCount = (int)nodes.size();

    coolingTypeFunctions.insert(std::make_pair(SimulatedAnnealing::EXPONENTIAL,SimulatedAOperators::exponentialDecay));
    coolingTypeFunctions.insert(std::make_pair(SimulatedAnnealing::LINEAR,SimulatedAOperators::linearDecay));


}

void SimulatedAnnealing::load() {}

void SimulatedAnnealing::select(Algorithm::algoType) {}

void SimulatedAnnealing::select(SimulatedAnnealing::coolingTypes coolType, SimulatedAnnealing::coolingLocation coolLocation,int initemp) {

    activeCoolingType = coolingTypeFunctions[coolType];

    coolTypeStr = coolingTypeStrings[coolType];
    coolLocationStr = coolingLocationStrings[coolLocation];

    activeCoolingLocation = coolLocation;

    initialTemperature = initemp;

}

void SimulatedAnnealing::execute() {
    auto start = clock::now();
    solution = simulated_annealing();
    auto end = clock::now();
    executionTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
}

void SimulatedAnnealing::display() {
    std::cout << "\n\nSimulated Annealing\n"
              << "Cooling Method: " << coolTypeStr << " Cooling Location: " << coolLocationStr << "\n"
              << "Solution Path: " << solution.second << "\n"
              << "Solution Cost: " << solution.first << "\n"
              << "Iterations surpassed: " << totalIterations << "\n"
              << "Execution Time: " << executionTime.count() << " microseconds" << std::endl;
}

void SimulatedAnnealing::output() {}



std::pair<float,std::string> SimulatedAnnealing::simulated_annealing() {
    neighborhood_t neighborhood;
    solution_t bestSoFar;
    solution_t currentSolution;
    int temperature,
        iterations = 0;

    std::string bestPath = "";

    auto start = clock::now();


    temperature = initialTemperature;
    currentSolution = generateInitialSolution();
    calculateSolutionFitness(currentSolution);
    bestSoFar = currentSolution;

    std::stringstream ss;
    copy(bestSoFar.second.begin(),bestSoFar.second.end(),std::ostream_iterator<int>(ss," "));
    bestPath = "";
    bestPath += "1 " +  ss.str() + "1";

    do{
        if(bestPath == dynamicSolution || bestPath == reversedSolution){
            totalIterations = iterations;
            return std::make_pair(bestSoFar.first,bestPath);
        }

        iterations++;

        //generate a neighborhood for the current solution
        neighborhood = generateNeighborhood(currentSolution);

        //update fitness of all neighbors
        calculateFitness(neighborhood);

        //move to a new solution from the neighborhood
        currentSolution = selectNeighbor(currentSolution,neighborhood,temperature,iterations);

        //if current solution is better than best so far, update best so far
        if(currentSolution.first < bestSoFar.first){
            std::stringstream tempSS;
            ss.swap(tempSS);
            bestSoFar = currentSolution;
            copy(bestSoFar.second.begin(),bestSoFar.second.end(),std::ostream_iterator<int>(ss," "));
            bestPath = "";
            bestPath += "1 " +  ss.str() + "1";
            auto currTime = clock::now();
            std::cout << bestPath << " Cost: " << bestSoFar.first << " " << std::chrono::duration_cast<std::chrono::microseconds>(currTime - start).count()  << std::endl;
            ss.clear();
        }

        if(activeCoolingLocation == EVERYITERATION){
            //decrease temperature with active cooling method
            activeCoolingType(initialTemperature,temperature,nodeCount,iterations);
        }

    }
    while(temperature > 1);

    totalIterations = iterations;

    std::stringstream tempSS;
    ss.swap(tempSS);
    copy(bestSoFar.second.begin(),bestSoFar.second.end(),std::ostream_iterator<int>(ss," "));
    bestPath = "";
    bestPath += "1 " +  ss.str() + "1";
    return std::make_pair(bestSoFar.first,bestPath);

}


SimulatedAnnealing::solution_t SimulatedAnnealing::generateInitialSolution() {
    std::vector<int> tempSol = nodes;

    random_shuffle(tempSol.begin(),tempSol.end());

    return std::make_pair(0.0,tempSol);

}

SimulatedAnnealing::neighborhood_t SimulatedAnnealing::generateNeighborhood(solution_t current) {
    neighborhood_t newNeighborhood;

    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> index_range(0,(int)(current.second.size()) - 1);

    //add neighbors to neighborhood until size of nodes * 10 is reached
    for(int i = 0,j = 0; i < (current.second.size() * current.second.size()); i++){
        //initially set newSol to the original
        std::vector<int> newSol = current.second;

        if(j == newSol.size())
            j = 0;

        //swap index j with a random index
        int randIndex = index_range(generator);
        int temp = newSol[j];
        newSol[j] = newSol[randIndex];
        newSol[randIndex] = temp;

        j++;

        //add newSol to the neighborhood
        newNeighborhood.push_back(std::make_pair(0.0,newSol));
    }

    return newNeighborhood;
}

SimulatedAnnealing::solution_t SimulatedAnnealing::selectNeighbor(SimulatedAnnealing::solution_t current, SimulatedAnnealing::neighborhood_t neighborhood, int& temperature,int iterations) {
    solution_t potentialSolution;
    float probability;

    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> index_range(0,(int)(neighborhood.size()) - 1);
    std::uniform_real_distribution<float> probability_range(0.0,1.0);

    int randIndex = index_range(generator);

    //choose a solution out of the neighborhood at random
    potentialSolution = neighborhood[randIndex];

    //if potential solution is better than the current, return potential solution to replace current
    if(potentialSolution.first < current.first)
        return potentialSolution;

    //calculate probability to move to worse solution using SA equation:          P = e^ -( f(s2) - f(s1) ) / T )
    probability = exp(-(potentialSolution.first - current.first) / temperature);

    if(activeCoolingLocation == EVERYWORSE)
        activeCoolingType(initialTemperature,temperature,current.second.size(),iterations);

    float prob = probability_range(generator);

    //if prob is in the range of probability, move to the potential solution, else stay at current solution
    if(prob <= probability)
        return potentialSolution;
    else
        return current;




}


void SimulatedAnnealing::calculateSolutionFitness(SimulatedAnnealing::solution_t & solution) {
    float fitness = 0.0;
    //calculate distance from start node to second node
    fitness += calculateDistance(searchGraph->adjList[1],searchGraph->adjList[solution.second[0]]);
    //for each node.. calculate distance from that node to its adjacent node
    for(int i = 0; i < solution.second.size() - 1;i++){
        std:: tuple<float,float,float> n1 = searchGraph->operator[](solution.second[i]), n2 = searchGraph->operator[](solution.second[i + 1]);
        fitness += calculateDistance(n1,n2);

    }
    //calculate distance from last node back to start node
    fitness += calculateDistance(searchGraph->operator[](solution.second[solution.second.size() - 1]),searchGraph->operator[](1));
    solution.first = fitness;
}

void SimulatedAnnealing::calculateFitness(SimulatedAnnealing::neighborhood_t & neighborhood) {
    //solution_t best = std::make_pair(INF,std::vector<int>(1));

    for(auto& solution : neighborhood){
        float fitness = 0.0;
        //calculate distance from start node to second node
        fitness += calculateDistance(searchGraph->adjList[1],searchGraph->adjList[solution.second[0]]);
        //for each node.. calculate distance from that node to its adjacent node
        for(int i = 0; i < solution.second.size() - 1;i++){
            std:: tuple<float,float,float> n1 = searchGraph->operator[](solution.second[i]), n2 = searchGraph->operator[](solution.second[i + 1]);
            fitness += calculateDistance(n1,n2);

        }
        //calculate distance from last node back to start node
        fitness += calculateDistance(searchGraph->operator[](solution.second[solution.second.size() - 1]),searchGraph->operator[](1));
        solution.first = fitness;

//        //if current chromosome's fitness is better than current best's then replace best with the current
//        if(fitness < best.first)
//            best = solution;
    }

   // return best;
}

float SimulatedAnnealing::calculateDistance(std::tuple<float, float, float> n1, std::tuple<float, float, float> n2) {
    float distance = 0.0;
    float x1 = std::get<0>((n1)) , y1 = std::get<1>((n1)) , z1 = std::get<2>((n1));
    float x2 = std::get<0>((n2)) , y2 = std::get<1>((n2)) , z2 = std::get<2>((n2));

    //use distance formula to calculate distance
    distance  += sqrt((pow((x1 - x2),2.0)) + (pow((y1 - y2),2.0)) + (pow((z1 - z2),2.0)));

    return distance;
}
