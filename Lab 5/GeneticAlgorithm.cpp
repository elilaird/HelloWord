

#include "GeneticAlgorithm.h"


GeneticAlgorithm::GeneticAlgorithm() {

}

GeneticAlgorithm::GeneticAlgorithm(Graph * g) {
    searchGraph = g;

    dynamicSolution = dynamicSolutions[searchGraph->size() - 1];
    reversedSolution = dynamicSolution;
    std::reverse(reversedSolution.begin(),reversedSolution.end());

    std::chrono::duration<int,std::micro> time(71739785);
    dynamicTime = time;

    //add each node to nodes vector
    for(auto n : g->adjList){
        if(n.first != 1)
            nodes.push_back(n.first);
    }

    //insert selection and crossover functions into vectors of function pointers
    selections.insert(std::make_pair(GeneticAlgorithm::ROULETTE,GeneticOperator::_rouletteSelection));
    selections.insert(std::make_pair(GeneticAlgorithm::ELITE,GeneticOperator::_eliteSelection));
    crossovers.insert(std::make_pair(GeneticAlgorithm::ORDERED,GeneticOperator::_orderedCrossover));
    crossovers.insert(std::make_pair(GeneticAlgorithm::HALFNHALF,GeneticOperator::_halfCrossover));

}

void GeneticAlgorithm::load() {}

void GeneticAlgorithm::select(Algorithm::algoType) {}

void GeneticAlgorithm::execute() {
    auto start = clock::now();
    solution = genetic_algorithm();
    auto end = clock::now();
    executionTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
}

void GeneticAlgorithm::display() {
    std::cout << "\n\nGenetic Algorithm\n"
              << "Selection Type: " << selectionStr << " | Crossover Type: " << crossoverStr << "\n"
              << "Mutation Swap Amount: " << mutationAmount << " | Mutation Probability: " << mutationPercent << "%\n"
              << "Solution Path: " << solution.second << "\n"
              << "Solution Cost: " << solution.first << "\n"
              << "Epocs surpassed: " << totalEpocs << "\n"
              << "Execution Time: " << executionTime.count() << " microseconds" << std::endl;
}

void GeneticAlgorithm::output() {}

void GeneticAlgorithm::select(selectionTypes selectionType, crossoverTypes crossoverType, float percent,int amount) {
    //set selection types,crossover types, mutation percentage, and mutation amount
    activeSelection = selections[selectionType];
    activeCrossOver = crossovers[crossoverType];
    mutate = GeneticOperator::_mutate;
    mutationPercent = percent / 100.0;
    mutationAmount = amount;

    selectionStr = selectionStrings[selectionType];
    crossoverStr = crossoverStrings[crossoverType];

}


//utility functions
GeneticAlgorithm::population_t GeneticAlgorithm::generateInitialPopulation() {
    population_t initPopulation;
    std::vector<int> tempChromosome;


    //initial population is the number of nodes * 10
    initPopulation.push_back(std::make_pair(0.0,nodes));
    for(int i = 0; i < (nodes.size() * 10) - 1; i ++){
        tempChromosome = nodes;
        random_shuffle(tempChromosome.begin(),tempChromosome.end());    //create random permutation
        initPopulation.push_back(std::make_pair(0.0,tempChromosome));   //add new permutation to population
    }

    return initPopulation;

}

GeneticAlgorithm::chromosome_t GeneticAlgorithm::calculateFitness(GeneticAlgorithm::population_t & population) {

    chromosome_t best = std::make_pair(INF,std::vector<int>(1));

    for(auto& chromosome : population){
        float fitness = 0.0;
        //calculate distance from start node to second node
        fitness += calculateDistance(searchGraph->adjList[1],searchGraph->adjList[chromosome.second[0]]);
        //for each node.. calculate distance from that node to its adjacent node
        for(int i = 0; i < chromosome.second.size() - 1;i++){
            std:: tuple<float,float,float> n1 = searchGraph->operator[](chromosome.second[i]), n2 = searchGraph->operator[](chromosome.second[i + 1]);
            fitness += calculateDistance(n1,n2);

        }
        //calculate distance from last node back to start node
        fitness += calculateDistance(searchGraph->operator[](chromosome.second[chromosome.second.size() - 1]),searchGraph->operator[](1));
        chromosome.first = fitness;

        //if current chromosome's fitness is better than current best's then replace best with the current
        if(fitness < best.first)
            best = chromosome;
    }

    return best;

}

std::pair<float, std::string> GeneticAlgorithm::genetic_algorithm() {
    int epocs = 0;
    population_t population;
    chromosome_t bestSoFar;
    chromosome_t temp;
    std::string bestPath = "";

    //start clock
    auto start = clock::now();

    //initialize population
    population = generateInitialPopulation();
    bestSoFar = calculateFitness(population);



    std::stringstream ss;
    copy(bestSoFar.second.begin(),bestSoFar.second.end(),std::ostream_iterator<int>(ss," "));
    bestPath = "";
    bestPath += "1 " +  ss.str() + "1";

    do{
        if(bestPath == dynamicSolution || bestPath == reversedSolution){
            totalEpocs = epocs;
            return std::make_pair(bestSoFar.first,bestPath);
        }
        epocs++;
        //use operators to generate a new population
        population_t newPopulation;
        newPopulation.push_back(bestSoFar);
        activeSelection(population,newPopulation);
        activeCrossOver(population,newPopulation);
        mutate(newPopulation,mutationPercent,mutationAmount);
        //calculate fitness for the new population
        temp = calculateFitness(newPopulation);
        if(temp.first < bestSoFar.first) {
            std::stringstream tempSS;
            ss.swap(tempSS);
            bestSoFar = temp;
            copy(bestSoFar.second.begin(),bestSoFar.second.end(),std::ostream_iterator<int>(ss," "));
            bestPath = "";
            bestPath += "1 " +  ss.str() + "1";
            auto currTime = clock::now();
            std::cout << bestPath << " Cost: " << bestSoFar.first << " " << std::chrono::duration_cast<std::chrono::microseconds>(currTime - start).count() << " Pop Size: " << population.size() << std::endl;
            ss.clear();

        }
        //use new population as the population for the next iteration
        population = newPopulation;

    }
    while((clock::now() - start) < dynamicTime);

    totalEpocs = epocs;

    std::stringstream tempSS;
    ss.swap(tempSS);
    copy(bestSoFar.second.begin(),bestSoFar.second.end(),std::ostream_iterator<int>(ss," "));
    bestPath = "";
    bestPath += "1 " +  ss.str() + "1";
    return std::make_pair(bestSoFar.first,bestPath);

}

//calculate the distance between two points
float GeneticAlgorithm::calculateDistance(std::tuple<float, float, float> n1, std::tuple<float, float, float> n2) {
    float distance = 0.0;
    float x1 = std::get<0>((n1)) , y1 = std::get<1>((n1)) , z1 = std::get<2>((n1));
    float x2 = std::get<0>((n2)) , y2 = std::get<1>((n2)) , z2 = std::get<2>((n2));

    //use distance formula to calculate distance
    distance  += sqrt((pow((x1 - x2),2.0)) + (pow((y1 - y2),2.0)) + (pow((z1 - z2),2.0)));

    return distance;
}