
#include "AlgoAnalyst.h"
#include "HamiltonianCircuit.h"
#include "GeneticAlgorithm.h"
#include "TabuSearch.h"
#include "ParticleSwarm.h"
#include "SimulatedAnnealing.h"
#include <iostream>
#include <random>

int main(int argc, const char * argv[]) {


    AlgoAnalyst analyst;
    analyst.load();


    ParticleSwarm* tsp = (ParticleSwarm*)(analyst.generateAlgorithm(AlgoAnalyst::PARTICLESWARM));
    tsp->select(1,4,1);

//    SimulatedAnnealing* tsp = (SimulatedAnnealing*)(analyst.generateAlgorithm(AlgoAnalyst::SA));
//    tsp->select(SimulatedAnnealing::EXPONENTIAL,SimulatedAnnealing::EVERYITERATION,12000);

    tsp->execute();
    tsp->display();




    return 0;
}
