

#include "Particle.h"

Particle::Particle() {
    fitness = 0.0;
}

Particle::Particle(float fit, std::vector<int> vel, std::vector<int> pos, Particle::solution_t best) {
    fitness = fit;
    velocity = vel;
    position = pos;
    pBest = best;

}

Particle::~Particle() {}

//Particle::Particle(Particle & p) {
//    this->fitness = p.fitness;
//    this->velocity = p.velocity;
//    this->position = p.position;
//    this->pBest = p.pBest;
//}

Particle& Particle::operator=(const Particle & p) {
    this->fitness = p.fitness;
    this->velocity = p.velocity;
    this->position = p.position;
    this->pBest = p.pBest;

    return *this;

}
