
#ifndef LAB_3_HAMILTONIANCIRCUIT_H
#define LAB_3_HAMILTONIANCIRCUIT_H

#include "Algorithm.h"
#include "TravelingSalesman.h"
#include "Graph.h"
#include <map>
#include <string>
#include <chrono>



class HamiltonianCircuit : public Algorithm {

private:

    //map used to store the optimal paths found by both implementations of traveling salesman problem
   // std::map<std::string,std::pair<float,std::string>> optimalPaths;

    //map of function pointers that point to the static TravelingSalesman functions
    static std::map<Algorithm::algoType ,std::pair<float,std::string>(*)(Graph*)> tfs_algorithms;

    //active function pointer acts as the current selected algorithm
    std::pair<float,std::string>(*activeAlgo)(Graph*);

    //calculated path and cost pair
    std::pair<float,std::string> pathAndCost;

    //graph used for the algorithms
    Graph* searchGraph;

    //type definition used to shorten length of code
    typedef std::chrono::high_resolution_clock clock;

    //high_resolution_clock instance used to store the total execution time
    std::chrono::microseconds executionTime;

    //string used to resemble the name of the active algorithm in string format
    std::string activeName;



public:
    HamiltonianCircuit();
    HamiltonianCircuit(Graph*);
    void load();
    void select(Algorithm::algoType);
    void execute();
    void output();
    void display();
   // std::pair<float,std::string> getPath(std::string);
    std::pair<float,std::string> getPathAndCost();

};


#endif //LAB_3_HAMILTONIANCIRCUIT_H
