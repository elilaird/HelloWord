

#include "SimulatedAOperators.h"


void SimulatedAOperators::exponentialDecay(int initialTemp,int & temperature, int nodeCount,int iterations) {
    //using a modified exponential decay function, decrease temperature ...      N(t) = N0 * e^ -(1/nodeCount)t - 1

    nodeCount = nodeCount * 100;
    temperature = (int)((float)initialTemp * exp(-(1/(float)nodeCount) * (float)iterations));
}

void SimulatedAOperators::linearDecay(int,int & temperature,int nodes,int iterations) {
    temperature -= 1;
}