
#ifndef LAB_3_TABUOPERATORS_H
#define LAB_3_TABUOPERATORS_H

#include <vector>
#include <string>
#include <cmath>
#include <random>
#include <algorithm>

class TabuOperators {

public:

    typedef std::pair<float,std::vector<int>> solution_t;
    typedef std::vector<solution_t> neighborhood_t;

    static neighborhood_t _neighborhoodCrossover(solution_t,int);
    static neighborhood_t _neighborhoodSwap(solution_t,int);

};


#endif //LAB_3_TABUOPERATORS_H
