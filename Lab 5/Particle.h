
#ifndef LAB_5_PARTICLE_H
#define LAB_5_PARTICLE_H

#include <vector>

class Particle {

public:
    typedef std::pair<float,std::vector<int>> solution_t;

    float fitness;
    std::vector<int> velocity;
    std::vector<int> position;
    solution_t pBest;



    Particle();
    ~Particle();
    Particle(float,std::vector<int>, std::vector<int>, solution_t);
    //Particle(Particle&);
    Particle& operator=(const Particle&);


};


#endif //LAB_5_PARTICLE_H
