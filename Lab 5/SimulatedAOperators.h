
#ifndef LAB_5_SIMULATEDAOPERATORS_H
#define LAB_5_SIMULATEDAOPERATORS_H

#include <math.h>

class SimulatedAOperators {

public:

    static void exponentialDecay(int,int&,int,int);
    static void linearDecay(int,int&,int,int);

};


#endif //LAB_5_SIMULATEDAOPERATORS_H
