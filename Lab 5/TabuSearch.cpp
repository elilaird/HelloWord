
#include "TabuSearch.h"

TabuSearch::TabuSearch() {}

TabuSearch::TabuSearch(Graph * g) {
    searchGraph = g;

    dynamicSolution = dynamicSolutions[searchGraph->size() - 1];
    dynamicCost = dynamicCosts[searchGraph->size() - 1];

    std::chrono::duration<int,std::micro> time(71739785);
    dynamicTime = time;

    reversedSolution = dynamicSolution;
    std::reverse(reversedSolution.begin(),reversedSolution.end());

    //add each node to nodes vector
    for(auto n : g->adjList){
        if(n.first != 1)
            nodes.push_back(n.first);
    }

    //insert crossover and swap function pointers into pointer map
    neighborhoodSelections.insert(std::make_pair(TabuSearch::CROSSOVER,TabuOperators::_neighborhoodCrossover));
    neighborhoodSelections.insert(std::make_pair(TabuSearch::SWAP,TabuOperators::_neighborhoodSwap));
}

void TabuSearch::load() {}

void TabuSearch::select(Algorithm::algoType) {}

void TabuSearch::select(TabuSearch::selectionTypes selectionType,size_t listSize) {
    //set neighborhoodSelector function pointer
    neighborhoodSelector = neighborhoodSelections[selectionType];

    generationType = neighborhoodStrings[selectionType];

    tabu_size = listSize;
}

void TabuSearch::execute() {
    auto start = clock::now();
    finalSolution = tabu_search();
    auto end = clock::now();
    executionTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

}

void TabuSearch::output() {}

void TabuSearch::display() {
    std::cout << "\n\nTabu Search Algorithm\n"
              << "Neighborhood Selection Type: " << generationType << "\n"
              << "Solution Path: " << finalSolution.second << "\n"
              << "Solution Cost: " << finalSolution.first << "\n"
              << "Iterations surpassed: " << totalIterations << "\n"
              << "Execution Time: " << executionTime.count() << " microseconds" << std::endl;
}


//Tabu Functions

std::pair<float,std::string> TabuSearch::tabu_search() {
    int count = 0, neighborhoodSize = nodes.size() * 2;
    neighborhood_t neighborhood;
    solution_t bestSoFar;
    solution_t localBest;
    std::string bestPath = "";

    //start clock
    auto start = clock::now();

    //initialize population
    solution_t currSolution = generateInitialSolution();
    neighborhood.push_back(currSolution);
    bestSoFar = calculateBest(neighborhood);
    currSolution = bestSoFar;

    do{
        count++;



        //if current solution is better than the best so far update best so far
        if(currSolution.first < bestSoFar.first){
            bestSoFar = currSolution;
            bestPath = solutionToString(bestSoFar);
            auto currTime = clock::now();
            std::cout << bestPath << " Cost: " << bestSoFar.first <<" Time: " << std::chrono::duration_cast<std::chrono::microseconds>(currTime - start).count() << std::endl;

        }

        //check if solution is found
        if(bestPath == dynamicSolution || bestPath == reversedSolution || bestSoFar.first == dynamicCost){
            totalIterations = count;
            return std::make_pair(bestSoFar.first,bestPath);
        }


        //add current solution to the Tabu list
        addToTabu(currSolution);

        //generate neighborhood for current solution
        neighborhood = neighborhoodSelector(currSolution,neighborhoodSize);

        //calculate score of each neighbor and choose best
        localBest = calculateBest(neighborhood);

        //check if localBest is in the Tabu list
        if(checkTabu(localBest)){
            //std::cout << "In Tabu" << std::endl;
            bool inTabu = true;
            //check if neighbors are in the Tabu list
            for(auto n : neighborhood) {
                if (!checkTabu(n)) {                    //if not in tabu list, visit it and continue algorithm
                    currSolution = n;
                    continue;
                }
            }
            if(inTabu){                                 //if all are in Tabu list call exitLocalMin to mutate the localBest
                exitLocalMin(localBest);
            }
        }

        //visit localBest solution
        currSolution = localBest;
        count++;
    }
    while((clock::now() - start) < dynamicTime);

    bestPath = solutionToString(bestSoFar);
    totalIterations = count;

    return std::make_pair(bestSoFar.first,bestPath);


}

TabuSearch::solution_t TabuSearch::generateInitialSolution() {
    solution_t initSolution;

    //set temp to the nodes in the searchGraph
    std::vector<int> temp = nodes;

    //shuffling temp
    random_shuffle(temp.begin(),temp.end());

    initSolution = std::make_pair(0.0,temp);

    return initSolution;

}

TabuSearch::solution_t TabuSearch::calculateBest(TabuSearch::neighborhood_t& n) {
    //calculate the score for each solution and sort them.
    //choose a best solution

    for(auto& sol : n){
        float score = 0.0;

        //calculate distance from start node to second node
        score += calculateDistance(searchGraph->adjList[1],searchGraph->adjList[sol.second[0]]);
        //for each node calculate from it to its adjacent node
        for(int i = 0; i < sol.second.size() - 1;i++){
            std:: tuple<float,float,float> n1 = searchGraph->operator[](sol.second[i]), n2 = searchGraph->operator[](sol.second[i + 1]);
            score += calculateDistance(n1,n2);
        }

        //calculate distance from last node back to start node
        score += calculateDistance(searchGraph->operator[](sol.second[sol.second.size() - 1]),searchGraph->operator[](1));
        sol.first = score;
    }

    //sort neighborhood from least to greatest
    std::sort(n.begin(),n.end());

    return n[0];


}

void TabuSearch::exitLocalMin(TabuSearch::solution_t & localMin) {
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> index_range(0,localMin.second.size() - 1);
    int swapCount = 0, swapAmount = localMin.second.size();

    while(swapCount < swapAmount){
        //generate two random indexes that will be swapped
        int index1 = index_range(generator), index2 = index_range(generator);

        //swap two indexes
        int temp = localMin.second[index1];
        localMin.second[index1] = localMin.second[index2];
        localMin.second[index2] = temp;

        swapCount++;
    }

   // random_shuffle(localMin.second.begin(),localMin.second.end());

}


//Tabu List Functions
bool TabuSearch::checkTabu(TabuSearch::solution_t sol) {
    std::string sol_str = solutionToString(sol);
    for(std::string n : tabuList){
        if(n == sol_str)
            return true;
    }
    return false;
}

void TabuSearch::addToTabu(TabuSearch::solution_t solution) {
    std::string sol = solutionToString(solution);
    tabuList.push_back(sol);

    //check if Tabu List has reached capacity
    if(tabuList.size() > tabu_size){
        //while Tabu List length is greater than the set size, erase the oldest element
        while(tabuList.size() > tabu_size){
            tabuList.erase(tabuList.begin());
        }
    }
}

//Utility Functions
std::string TabuSearch::solutionToString(TabuSearch::solution_t sol) {
    std::stringstream ss;
    std::string sol_str = "";

    copy(sol.second.begin(),sol.second.end(),std::ostream_iterator<int>(ss," "));

    sol_str += "1 " + ss.str() + "1";
    ss.clear();

    return sol_str;

}

float TabuSearch::calculateDistance(std::tuple<float, float, float> n1, std::tuple<float, float, float> n2) {
    float distance = 0.0;
    float x1 = std::get<0>((n1)) , y1 = std::get<1>((n1)) , z1 = std::get<2>((n1));
    float x2 = std::get<0>((n2)) , y2 = std::get<1>((n2)) , z2 = std::get<2>((n2));

    //use distance formula to calculate distance
    distance  += sqrt((pow((x1 - x2),2.0)) + (pow((y1 - y2),2.0)) + (pow((z1 - z2),2.0)));

    return distance;
}