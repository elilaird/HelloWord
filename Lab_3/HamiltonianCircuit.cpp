
#include "HamiltonianCircuit.h"


//forward declaration of static map
std::map<Algorithm::algoType ,std::pair<float,std::string>(*)(Graph*)> HamiltonianCircuit::tfs_algorithms;

HamiltonianCircuit::HamiltonianCircuit() {
    tfs_algorithms.insert(std::make_pair(Algorithm::TSPNAIVE,TravelingSalesman::tfs_naive));
    tfs_algorithms.insert(std::make_pair(Algorithm::TSPDYNAMIC,TravelingSalesman::tfs_dynamic));
}

HamiltonianCircuit::HamiltonianCircuit(Graph* graph) {

    //load map of function pointers with functions
    tfs_algorithms.insert(std::make_pair(Algorithm::TSPNAIVE,TravelingSalesman::tfs_naive));
    tfs_algorithms.insert(std::make_pair(Algorithm::TSPDYNAMIC,TravelingSalesman::tfs_dynamic));

    //assign graph to the map passed in
    searchGraph = graph;


}

void HamiltonianCircuit::load() {}

void HamiltonianCircuit::select(Algorithm::algoType type) {

    //assign activeAlgo to corresponding algorithm in tfs_algorithms
    activeAlgo = tfs_algorithms.at(type);

    //assign the activeName to the corresponding string
    if(type == Algorithm::TSPNAIVE)
        activeName = "NAIVE TSP";
    else if (type == Algorithm::TSPDYNAMIC)
        activeName = "DYNAMIC TSP";
    else activeName = "ALGORITHM";

}

void HamiltonianCircuit::execute() {

    auto start = clock::now();
    pathAndCost = activeAlgo(searchGraph);
    auto end = clock::now();
    executionTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
}

void HamiltonianCircuit::output() {}

void HamiltonianCircuit::display() {

    std::cout << "Algorithm: " << activeName << "\n"
              << "Path: " << pathAndCost.second << "\n"
              << "Cost: " << pathAndCost.first << "\n"
              << "Execution Time: " << executionTime.count() << std::endl;
}

std::pair<float,std::string> HamiltonianCircuit::getPath(std::string type) {
   // return optimalPaths.at(type);
}

std::pair<float,std::string> HamiltonianCircuit::getPathAndCost() {
    return pathAndCost;
}