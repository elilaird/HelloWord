
#include "AlgoAnalyst.h"
#include "HamiltonianCircuit.h"

int main(int argc, const char * argv[]) {



    AlgoAnalyst analyst;
    analyst.load();

    HamiltonianCircuit* tsp = (HamiltonianCircuit*)(analyst.generateAlgorithm(AlgoAnalyst::HAMILTONIANCIRCUIT));
    tsp->select(Algorithm::TSPNAIVE);
    tsp->execute();
    tsp->display();
    tsp->select(Algorithm::TSPDYNAMIC);
    tsp->execute();
    tsp->display();

    return 0;
}
