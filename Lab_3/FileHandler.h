
#ifndef LAB_3_FILEHANDLER_H
#define LAB_3_FILEHANDLER_H

#include <map>
#include <fstream>
#include <sstream>

class FileHandler {

public:
    static std::map<int,std::tuple<float,float,float>> parse();
};


#endif //LAB_3_FILEHANDLER_H
