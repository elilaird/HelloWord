
#ifndef LAB_3_ALGORITHM_H
#define LAB_3_ALGORITHM_H



class Algorithm {

public:

    enum algoType {
        TSPNAIVE = 0,
        TSPDYNAMIC
    };

private:


    virtual void load() = 0;
    virtual void select(Algorithm::algoType) = 0;
    virtual void execute() = 0;
    virtual void output() = 0;
    virtual void display() = 0;


};


#endif //LAB_3_ALGORITHM_H
