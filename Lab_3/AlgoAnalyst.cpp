
#include "AlgoAnalyst.h"


AlgoAnalyst::AlgoAnalyst() {
    g = new Graph();
}

AlgoAnalyst::~AlgoAnalyst() {
    delete g;
}

Algorithm* AlgoAnalyst::generateAlgorithm(AlgoAnalyst::algoType type) {

    if(type == AlgoAnalyst::HAMILTONIANCIRCUIT)
        return new HamiltonianCircuit(g);
    else
        std::cout << "Error generating" << std::endl;
        exit(EXIT_FAILURE);

}

void AlgoAnalyst::load() {
    g->adjList = FileHandler::parse();


}

void AlgoAnalyst::output() {}