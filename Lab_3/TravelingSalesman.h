
#ifndef LAB_3_TRAVELINGSALESMAN_H
#define LAB_3_TRAVELINGSALESMAN_H

#include <string>
#include <map>
#include <stack>
#include <vector>
#include <iostream>
#include <math.h>
#include <algorithm>
#include "Graph.h"

#define INF 0x3f3f3f3f

class TravelingSalesman {

public:

    static std::pair<float,std::string> tfs_naive(Graph* graph);
    static std::pair<float,std::string> tfs_dynamic(Graph* graph);

private:

    static void tfs_naiveUtil(Graph* graph,std::vector<bool> visited,std::vector<std::vector<int>> &paths,std::stack<int> &path,int current,int numCount, int &callCount, int &numIter);
    static std::pair<float,std::string> tfs_dynamicUtil(Graph* graph, std::vector<int> relFrontier, int src,std::vector<std::vector<float>> &cache,int &numCalls);

    static int factorial(int num);
    static float calcDistance(std::tuple<float,float,float> ,std::tuple<float,float,float>);
    static std::pair<float,std::vector<int>> calcShortest(Graph* graph,std::vector<std::vector<int>> paths);
    static std::vector<int> storePath(std::vector<std::vector<int>> &paths, std::stack<int> path);

};


#endif //LAB_3_TRAVELINGSALESMAN_H
