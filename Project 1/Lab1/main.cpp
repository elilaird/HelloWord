#include <iostream>
#include "sort.h"
#include "datamaker.h"
using namespace std;

int main()
{
      //***DataMaker class if for writing data files***
    DataMaker data;
    data.write();

    //array of file names
    char* files[16] = {"Random10.txt","Random1000.txt","Random10000.txt","Random100000.txt"
                       ,"Reversed10.txt","Reversed1000.txt","Reversed10000.txt","Reversed100000.txt"
                       ,"twentyPercent10.txt","twentyPercent1000.txt","twentyPercent10000.txt","twentyPercent100000.txt"
                       ,"thirtyPercent10.txt","thirtyPercent1000.txt","thirtyPercent10000.txt","thirtyPercent100000.txt"};

    enum sortAlg {BUBBLE, MERGE, INSERTION, LAST};
    Algorithm* a;
    a = new Sort();
    for(int i = 0; i < 16;i++){
        a->Load(files[i]);
        for(int alg = BUBBLE; alg != LAST;alg++){
             a->Select(alg);
             a->Execute();

        }
        a->Stats();
    }
    //***Uncomment below to display
     //a->Display();
     a->Save();

    return 0;
}
