#include "sort.h"
#include <fstream>
#include <iostream>
#include "sortingalgo.h"


std::vector<void(*)(std::vector<int>&)> Sort::algorithms;

Sort::Sort(){  
    //pushes the sorting algorithms to the vector of function pointers
    algorithms.push_back(SortingAlgo<int>::bubble);
    algorithms.push_back(SortingAlgo<int>::merge);
    algorithms.push_back(SortingAlgo<int>::insertion);

    //resizes the vector of times to the correct size of algorithms
    sortTimes.resize(algorithms.size());
}

Sort::~Sort(){

}

void Sort::Load(char* fileName){
    //if dataSet has values.. clear the vector
   if(dataSet.size() > 0)
       dataSet.clear();
   std::ifstream f(fileName);
   int num,recordCount = 0;

   //store data from the file into dataset vector
   while(f >> num){
       dataSet.push_back(num);
       recordCount++;
   }
   _numRecords = recordCount;
   f.close();
}

void Sort::Execute(){
    auto start = clock::now();          //takes the start time
    activeAlgo(Sort::dataSet);          //executes algorithm
    auto end = clock::now();            //takes the end time
    sortTimes[_selection] = end - start;//adds the total time to times vector
    
}

void Sort::Display(){
    std::cout << "Solution: ";
    for(auto num : dataSet)
        std::cout << num << " ";
    std::cout << std::endl;
}

void Sort::Stats(){
    std::cout.precision(10);            //sets precision to 10 decimal places
    std::cout << "********************************************************" << std::endl;
    for(int i = 0; i < algorithms.size();i++){
        if(i == Algorithm::BUBBLE)
            std::cout << "\n\nAlgorithm: Bubble Sort" << std::endl;
        else if(i == Algorithm::MERGE)
            std::cout << "\n\nAlgorithm: Merge Sort" << std::endl;
        else if(i == Algorithm::INSERTION)
            std::cout << "\n\nAlgorithm: Insertion Sort" << std::endl;
        std::cout << "Execution Time: " << std::fixed << sortTimes[i].count() << "\nNumber of Records: " << _numRecords << std::endl;

    }

}

void Sort::Select(int chosen){
    _selection = chosen;
    activeAlgo = Sort::algorithms[chosen];
}

void Sort::Save(){
    std::ofstream file("Saved_Data.txt");

    if(file.is_open())
        for(int num : dataSet)
            file << num << std::endl;
    file.close();
}

void Sort::Configure(){}









