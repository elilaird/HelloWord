#ifndef DATAMAKER_H
#define DATAMAKER_H
#include <fstream>
#include <time.h>
#include <iostream>

class DataMaker
{
public:
    DataMaker();
    void random(int,int);
    void reversed(int,int);
    void twentyPercent(int,int);
    void thirtyPercent(int,int);
    void write();
};

#endif // DATAMAKER_H
