TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    sort.cpp \
    datamaker.cpp \
    tree.cpp

HEADERS += \
    sort.h \
    sortingalgo.h \
    datamaker.h \
    algorithm.h \
    tree.h
