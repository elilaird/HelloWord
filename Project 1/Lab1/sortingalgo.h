#ifndef SORTINGALGO_H
#define SORTINGALGO_H
#include <vector>
#include "tree.h"

template<typename T>
class SortingAlgo
{

public:


    SortingAlgo();

    static void bubble(std::vector<T>&);
    static void merge(std::vector<T>&);
    static void insertion(std::vector<T>&);
    static void BinaryTree(std::vector<T> &data);
    static void BinaryTreeSort(std::vector<T> &, int index);
private:
     static void _bubbleSort(std::vector<T>&);
     static void _mergeSort(std::vector<T>&,int,int);
     static void _merge(std::vector<T>&,int,int,int);
     static void _insertionSort(std::vector<T>&);

};


template<typename T>
void SortingAlgo<T>::bubble(std::vector<T> &vec){
    _bubbleSort(vec);
}

template<typename T>
void SortingAlgo<T>::merge(std::vector<T> &vec){
    _mergeSort(vec,0,vec.size() - 1);
}

template<typename T>
void SortingAlgo<T>::insertion(std::vector<T> &vec){
    _insertionSort(vec);
}





//****************Bubble Sort***************//


template<typename T>
void SortingAlgo<T>::_bubbleSort(std::vector<T> &vec){
    int len = vec.size();
    for(int i = 0; i < len - 1;i++){
        for(int j = 0; j < len - i - 1;j++){
            if(vec[j] > vec[j + 1]){ //if left int is less than right int then swap
                int temp = vec[j + 1];
                vec[j + 1] = vec[j];
                vec[j] = temp;
            }
        }
    }
}




//*****************Merge Sort****************//


template<typename T>
void SortingAlgo<T>::_mergeSort(std::vector<T> &vec,int l, int r){
    if(r > l){
        int mid = (l + r) / 2;
        _mergeSort(vec,l,mid);
        _mergeSort(vec,mid + 1,r);
        _merge(vec,l,mid,r);
    }
}

template<typename T>
void SortingAlgo<T>::_merge(std::vector<T> &vec, int l, int m, int r){
    int i, j, k, lenL = m - l + 1, lenR = r - m;

    //create temp arrays
    int left[lenL], right[lenR];

    //copy into temp arrays
    for(i = 0; i < lenL; i++)
        left[i] = vec[l + i];
    for(j = 0; j < lenR;j++)
        right[j] = vec[m + 1 + j];

    i = 0,j = 0,k = l; // i is index for first array, j is second, k is index for the merged array

    while( i < lenL && j < lenR){
        if(left[i] <= right[j]){
            vec[k] = left[i];
            i++;
        }
        else{
            vec[k] = right[j];
            j++;
        }
        k++;
    }

    //copy the rest of left and right subarray
    while(i < lenL)
        vec[k++] = left[i++];
    while(j < lenR)
        vec[k++] = right[j++];
}




//*************Insertion Sort*****************//



template<typename T>
void SortingAlgo<T>::_insertionSort(std::vector<T> &vec){

    int left, key, len = vec.size();
    for(int i = 1; i < len;i++){
        key = vec[i];
        left = i - 1;

        while(left >= 0 && vec[left] > key){
            vec[left + 1] = vec[left];
            left--;
        }
        vec[left + 1] = key;
    }
}




// ***********Binary Tree Sort***************//

template<typename T>
void SortingAlgo<T>::BinaryTree(std::vector<T> &data){
    SortingAlgo<T>::BinaryTreeSort(data,data.size());
}

template<typename T>
void SortingAlgo<T>::BinaryTreeSort(std::vector<T> &data, int index){
    struct Tree::Node * root = 0;
    root = Tree::InsertNode(root,data[0]);


    for(int i = 1; i < index;i++)
        Tree::InsertNode(root,data[i]);


    int i = 0;
    Tree::CopySortedTree(root,data,i);
}





















#endif // SORTINGALGO_H
