#ifndef ALGORITHM_H
#define ALGORITHM_H
#include <vector>

class Algorithm {
public:
    enum sortAlgs {BUBBLE,MERGE,INSERTION};
    virtual void Load(char*) = 0;     //Takes a filename as and can read input data file
    virtual void Execute() = 0;  //Executes the search algorithm
    virtual void Display() = 0;  //Prints solution to screen
    virtual void Stats() = 0;    //Prints algorithm name, execution time and number of records analyzed to screen in a readable format
    virtual void Select(int) = 0;   //enum or int or id passed as input and loads corresponding algorithm to interface
    virtual void Save() = 0;     //Saves solution to file path given as input
    virtual void Configue() {}     //Future expandability
    virtual ~Algorithm() {}
};


#endif // ALGORITHM_H
