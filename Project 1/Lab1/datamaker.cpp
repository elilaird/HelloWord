#include "datamaker.h"
#include "sortingalgo.h"

DataMaker::DataMaker()
{

}

void DataMaker::write(){
    enum sizes {TEN,THOUSAND,TENTHOUSAND,HUNDREDTHOUSAND};

    //call random to create random dataset for each size
    for(int i = TEN; i <= HUNDREDTHOUSAND;i++){
        if(i == TEN)
            random(10,TEN);
        else if(i ==THOUSAND)
            random(1000,THOUSAND);
        else if (i == TENTHOUSAND)
            random(10000,TENTHOUSAND);
        else if(i == HUNDREDTHOUSAND)
            random(100000,HUNDREDTHOUSAND);
    }

    //call reversed to create reversed sorted dataset for each size
    for(int i = TEN; i <= HUNDREDTHOUSAND;i++){
        if(i == TEN)
            reversed(10,TEN);
        else if(i ==THOUSAND)
            reversed(1000,THOUSAND);
        else if (i == TENTHOUSAND)
            reversed(10000,TENTHOUSAND);
        else if(i == HUNDREDTHOUSAND)
            reversed(100000,HUNDREDTHOUSAND);
    }

    //call random to create random dataset for each size
    for(int i = TEN; i <= HUNDREDTHOUSAND;i++){
        if(i == TEN)
            twentyPercent(10,TEN);
        else if(i ==THOUSAND)
            twentyPercent(1000,THOUSAND);
        else if (i == TENTHOUSAND)
            twentyPercent(10000,TENTHOUSAND);
        else if(i == HUNDREDTHOUSAND)
            twentyPercent(100000,HUNDREDTHOUSAND);
    }

    //call reversed to create reversed sorted dataset for each size
    for(int i = TEN; i <= HUNDREDTHOUSAND;i++){
        if(i == TEN)
            thirtyPercent(10,TEN);
        else if(i ==THOUSAND)
            thirtyPercent(1000,THOUSAND);
        else if (i == TENTHOUSAND)
            thirtyPercent(10000,TENTHOUSAND);
        else if(i == HUNDREDTHOUSAND)
            thirtyPercent(100000,HUNDREDTHOUSAND);
    }
}

void DataMaker::random(int size,int fileNamesIndex){
    char* fileNames[4] = {"Random10.txt","Random1000.txt","Random10000.txt","Random100000.txt"};
    std::ofstream outFile(fileNames[fileNamesIndex]);

    std::srand(std::time(NULL));

    if(outFile.is_open())
        for(int i = 0;i < size;i++)
            outFile << (std::rand() % 100000) << "\n";

    outFile.close();
}

void DataMaker::reversed(int size, int fileNamesIndex){
    char* fileNames[4] = {"Reversed10.txt","Reversed1000.txt","Reversed10000.txt","Reversed100000.txt"};
    std::ofstream outFile(fileNames[fileNamesIndex]);

    if(outFile.is_open())
        for(int i = size; i > 0;i--)
            outFile << i << "\n";
}

void DataMaker::twentyPercent(int size, int fileNamesIndex){
    char* fileNames[4] = {"twentyPercent10.txt","twentyPercent1000.txt","twentyPercent10000.txt","twentyPercent100000.txt"};
    std::ofstream outFile(fileNames[fileNamesIndex]);

    std::srand(std::time(NULL));

   //*** 20% of a dataset is that dataset's size divided by 5... to get twenty percent random, find 5 rand values and fill in the dataset with thoughs values until full because each set of five takes up 20%
    int uniques[5]; // array to store the unique values

    //populates the array with uniqueNums amount of random unique numbers
    for(int i = 0; i < 5;i++)
        uniques[i] = (std::rand() % 100000);

    int count = 0;
    while(count < size){
        for(int k = 0; k < 5;k++){
            outFile << uniques[k] << "\n";
            count ++;
        }
    }
}

void DataMaker::thirtyPercent(int size, int fileNamesIndex){
    char* fileNames[4] = {"thirtyPercent10.txt","thirtyPercent1000.txt","thirtyPercent10000.txt","thirtyPercent100000.txt"};
    std::ofstream outFile(fileNames[fileNamesIndex]);

    std::srand(std::time(NULL));

    int randSize = size * .3;

    if(outFile.is_open()){
        for(int i = 0; i < randSize;i++)
            outFile << (std::rand() % 100000) << "\n";
        for(int j = 0; j < (size - randSize);j++)
            outFile << j << "\n";
    }


}





