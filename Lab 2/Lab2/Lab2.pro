TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_MAC_SDK = macosx10.13
SOURCES += \
        main.cpp \
    sort.cpp \
    tree.cpp \
    search.cpp \
    searchalgo.cpp \
    node.cpp \
    adjlist.cpp \
    adjmatrix.cpp \
    treenode.cpp

HEADERS += \
    algorithm.h \
    sort.h \
    sortingalgo.h \
    tree.h \
    search.h \
    searchalgo.h \
    node.h \
    graph.h \
    adjlist.h \
    adjmatrix.h \
    treenode.h
