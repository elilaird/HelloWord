#include "node.h"

Node::Node(){
    id = -1;
    weight = 0;
    //position = std::make_tuple(0.0,0.0,0.0);
}


Node::Node(int num)
{
    id = num;
    weight = 0;
    position = std::make_tuple(0.0,0.0,0.0);
}

Node::Node(int _id, float cost){
    id = _id;
    weight = cost;
}

Node::~Node(){

}

void Node::addToDest(Node &n){
    destinations.push_back(n);
}

void Node::print(){
    std::cout << "Id: " << id << "\nWeight: " << weight << "\nPosition: "
              << std::get<0>(position) << ", " << std::get<1>(position) << ", " << std::get<2>(position)
              <<"\nDestinations: ";
    for(auto n : destinations)
        std:: cout << n.id << " ";
      std::cout << std::endl;
}

std::vector<Node>& Node::getDestinations(){
    return destinations;
}



std::tuple<float,float,float>& Node::getPosition(){
    return position;
}

void Node::setPosition(std::tuple<float, float, float> tup){
    position = tup;
}

int Node::getId(){
    return id;
}

float Node::getWeight(){
    return weight;
}

void Node::setId(int x){
    id = x;
}

void Node::setWeight(int x){
    weight = x;
}



Node& Node::operator =(const Node& n){
    this->id = n.id;
    this->weight = n.weight;
    this->destinations = n.destinations;
    this->position = n.position;

    return *this;

}

float Node::getCost(int dest){
    for(auto n : destinations){
        if(n.id == dest)
            return n.weight;
    }
    return 0.0;
}


















