#include "adjlist.h"
#include <iostream>
AdjList::AdjList(){

}

AdjList::~AdjList(){

}


void AdjList::insertNode(Node& n){
    alist.insert(std::make_pair(n.getId(),n));

}

void AdjList::insertWeight(int srcID,int dest,float wgt){
    for(auto& n : alist[srcID].getDestinations()){
        if(n.getId() ==  dest){
            n.setWeight(wgt);
            break;
        }
    }

}

Node& AdjList::operator[](int x){
    return alist[x];
}

size_t AdjList::getSize(){
    return alist.size();
}

void AdjList::setGraphSize(int x){
    graphSize = x;
}
