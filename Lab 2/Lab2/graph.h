#ifndef GRAPH_H
#define GRAPH_H
#include "node.h"

class Graph
{
public:
    virtual ~Graph(){}
    virtual void insertNode(Node&) = 0;
    virtual Node& operator[](int) = 0;
    virtual size_t getSize() = 0;
    virtual void insertWeight(int,int,float) = 0;
    virtual void setGraphSize(int) = 0;
};

#endif // GRAPH_H
