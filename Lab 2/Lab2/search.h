#ifndef SEARCH_H
#define SEARCH_H
#include "algorithm.h"
#include "searchalgo.h"
#include "adjlist.h"
#include "adjmatrix.h"

#include <chrono>
#include <math.h>
#include <iterator>
#include <algorithm>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <list>
#include <fstream>

class Search : public Algorithm
{
public:

    Search();
    virtual ~Search();
    void Load(char*);
    void Load();
    void Execute();
    void Execute(int src, int dest);
    void Display();
    void Stats();
    void Select(int);
    void Select(int,int);
    void Save();
    void Configure();
    float findCost();
    float findDistance();

private:
    static std::vector<std::list<Node>(*)(Graph* g,int src, int dest)> searchAlgorithms;
    std::list<Node>(*activeAlgo)(Graph* g,int src, int dest);
    SearchAlgo* algo;
    Graph* adjList, *adjMatrix;
    Graph* activeGraph;
    std::list<Node> path;
    float totalCost, totalDistance;
    int visitedTotal, asrc, adest;

    //type definitions used to make code shorter for the timers
    typedef std::chrono::high_resolution_clock clock;
    std::vector<std::chrono::duration<double> > searchTimes;
    std::chrono::duration<double> currTime;

    std::string algNames[4] = {"DFS ITERATIVE","DFS RECURSIVE" ,"BFS ITERATIVE","BFS RECURSIVE"};
    std::string graphNames[2] = {"ADJACENCY LIST","ADJACENCY MATRIX"};

    std::string currAlgName, currGraphName;

    std::ofstream file;//("Output.txt");

};

#endif // SEARCH_H
