#ifndef TREENODE_H
#define TREENODE_H
#include <vector>


class TreeNode
{
public:
    TreeNode();
    std::vector<TreeNode>& getChildren();
    void pushToChildren(TreeNode& tn);
private:
    std::vector<TreeNode> children;
};

#endif // TREENODE_H
