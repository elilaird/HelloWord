#ifndef NODE_H
#define NODE_H
#include <tuple>
#include <vector>
#include <iostream>

class Node
{
public:
    Node();
    Node(int);
    Node(int,float);
    ~Node();
    std::vector<Node>& getDestinations();
    std::tuple<float,float,float>& getPosition();
    void setPosition(std::tuple<float,float,float> tup);
    int getId();
    float getWeight();
    void setWeight(int x);
    void addToDest(Node& n);
    void print();
    void setId(int x);
    Node& operator =(const Node& n);
    float getCost(int dest);

private:
    int id;
    float weight;
    std::vector<Node> destinations;
    std::tuple<float,float,float> position;





};

#endif // NODE_H
