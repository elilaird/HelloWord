#include "search.h"

std::vector<std::list<Node>(*)(Graph* g,int src, int dest)> Search::searchAlgorithms;
int SearchAlgo::totalVisited;

Search::Search(){
    adjList = new AdjList();
    adjMatrix = new AdjMatrix();

    //add function pointers to searchAlgorithms vector
    searchAlgorithms.push_back(SearchAlgo::DFSIterative);
    searchAlgorithms.push_back(SearchAlgo::DFSRecursive);
    searchAlgorithms.push_back(SearchAlgo::BFSIterative);
    searchAlgorithms.push_back(SearchAlgo::BFSRecursive);

    SearchAlgo::totalVisited = 0;

    file.open("Output.txt");

}

Search::~Search(){
    if(adjList != nullptr)
        delete adjList;
    if(adjMatrix != nullptr)
        delete adjMatrix;
    file.close();
}

void Search::Load(char*){

}

void Search::Load(){
    std::string line;
    int srcID,dest,nodeCount = 0;
    float x,y,z,weight;
    std::vector<Node> destIds;
    std::stringstream ss;
    size_t comma;
    std::ifstream file("graph.txt");

    //count how many nodes there are in the file
    while(std::getline(file,line))
        nodeCount++;
    file.close();


    adjMatrix->setGraphSize(nodeCount);
    file.open("graph.txt");
    //reads in graph.txt and inserts nodes and connections to activeGraph
    while(std::getline(file,line)){

        comma = line.find(',');
        while(comma != std::string::npos){
            line.replace(comma,1," ");
            comma = line.find(',');
        }
        ss << line;

        //extract source Id
        ss >> srcID;

        // extract destination ids
        while(ss >> dest){
           // ss >> dest;
            destIds.push_back(Node(dest));
        }

        //create node & set destinations vector to destIds
        Node n(srcID);
        n.getDestinations() = destIds;

        //insert the node into the graph
        adjList->insertNode(n);
        adjMatrix->insertNode(n);
        ss.clear();
        destIds.clear();
    }
    file.close();

    file.open("positions.txt");

    //reads in positions.txt and inserts the positions to corresponding nodes
    while(std::getline(file,line)){

        //find first comma index
       comma = line.find(',');
       while(comma != std::string::npos){
           line.replace(comma,1," ");
           comma = line.find(',');
       }
       ss << line;
       ss >> srcID;

       //extract coordinates
       ss >> x;
       ss >> y;
       ss >> z;

       //set position
       adjList->operator [](srcID).setPosition(std::make_tuple(x,y,z));
       adjMatrix->operator [](srcID).setPosition(std::make_tuple(x,y,z));
       ss.clear();
    }
    file.close();

    file.open("weights.txt");

    //reads in weight.txt and inserts the weights to corresponding node connections
    while(std::getline(file,line)){

        //find first comma index
       comma = line.find(',');
       while(comma != std::string::npos){
           line.replace(comma,1," ");
           comma = line.find(',');
       }
       ss << line;
       ss >> srcID;
       ss >> dest;

       //extract weight
       ss >> weight;

       //find the dest node in the srcID node's dest vector and add the weight
       adjList->insertWeight(srcID,dest,weight);
       adjMatrix->insertWeight(srcID,dest,weight);

    ss.clear();
    }
    file.close();

        for(int i=1; i<adjList->getSize()+1; i++){
            std::cout << (adjList->operator[](i).getId()) << " | ";
            for(auto n : adjList->operator [](i).getDestinations()){
                std::cout << "Name: " << n.getId() << ",";
                std::cout << "Cost: " << n.getWeight() << ",";
            }
            std::cout << std::endl;
        }
        std::cout << "----------------------------------------------------" << std::endl;

        for(int i=1; i<adjMatrix->getSize()+1; i++){
            if(adjMatrix->operator [](i).getId() != -1)
                std::cout << (adjMatrix->operator[](i).getId()) << " | ";

            for(auto n : adjMatrix->operator [](i).getDestinations()){
                if(n.getId() != -1){
                    std::cout << "Name: " << n.getId() << ",";
                    std::cout << "Cost: " << n.getWeight() << ",";
                }
            }
            std::cout << std::endl;
        }

}

void Search::Execute(){

}
void Search::Execute(int src, int dest){
     asrc = src;
     adest = dest;
     SearchAlgo::totalVisited = 0;
     auto start = clock::now();          //takes the start time
     path = activeAlgo(activeGraph,src,dest);
     auto end = clock::now();            //takes the end time
     currTime = (end -start);
     searchTimes.push_back(end - start);//adds the total time to times vector
     totalCost = findCost();
     totalDistance = findDistance();
     visitedTotal = SearchAlgo::totalVisited;
}

void Search::Display(){   

    std::cout << "~~~~~~~~~~~~~~~~" << currAlgName << " " << currGraphName << "~~~~~~~~~~~~~~~~\n\n\n";
    std::cout << "Source: " << asrc << " Destination: " << adest << "\n\n" ;
    std::cout << "Path: ";
    for(auto n : path)
        std::cout << n.getId() << "->";
    std::cout << "\nTime: " << currTime.count();
    std::cout << "\nTotal Cost: " << totalCost;
    std::cout << "\nTotal Distance " << totalDistance;
    std::cout << "\nTotal Visited Nodes: " << visitedTotal;
    std::cout << "\n\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
    std::cout << std::endl;
}

void Search::Stats(){
   // std::ofstream file("output.txt");

    file << "~~~~~~~~~~~~~~~~" << currAlgName << " " << currGraphName << "~~~~~~~~~~~~~~~~\n\n\n";
    file << "Source: " << asrc << " Destination: " << adest << "\n\n" ;
    file << "Path: ";
    for(auto n : path)
        file << n.getId() << "->";
    file << "\nTime: " << currTime.count();
    file << "\nTotal Cost: " << totalCost;
    file << "\nTotal Distance " << totalDistance;
    file << "\nTotal Visited Nodes: " << visitedTotal;
    file << "\n\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";

}

void Search::Select(int choice){

}

void Search::Select(int alg, int graphNum){
    if(graphNum == 0){
        activeGraph = adjList;
        currGraphName = graphNames[graphNum];
    }
    else if(graphNum == 1){
        activeGraph = adjMatrix;
        currGraphName = graphNames[graphNum];
    }
    currAlgName = algNames[alg];
    activeAlgo = Search::searchAlgorithms[alg];
}

void Search::Save(){

}

void Search::Configure(){

}

float Search::findCost(){
    float cost = 0.0;
    typedef std::list<Node>::iterator iter;
    iter it = path.begin();
    for(size_t i = 0; i < path.size() - 1;i++){
        cost += (*it).getCost((*std::next(it,1)).getId());
        std::advance(it,1);
    }

    return cost;
}

float Search::findDistance(){
    float distance = 0.0;
    typedef std::list<Node>::iterator iter;
    iter it = path.begin();
    for(size_t i = 0; i < path.size() - 1;i++){
        iter next = std::next(it,1);
        float x1 = std::get<0>((*it).getPosition()) , y1 = std::get<1>((*it).getPosition()) , z1 = std::get<2>((*it).getPosition());
        float x2 = std::get<0>((*next).getPosition()) , y2 = std::get<1>((*next).getPosition()) , z2 = std::get<2>((*next).getPosition());
        distance  += (pow((x1 - x2),2.0)) + (pow((y1 - y2),2.0)) + (pow((z1 - z2),2.0));
        std::advance(it,1);
    }

    return distance;

}







































