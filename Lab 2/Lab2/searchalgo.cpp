#include "searchalgo.h"




SearchAlgo::SearchAlgo()
{

}

SearchAlgo::SearchAlgo(int selection){

}

std::list<Node> SearchAlgo::DFSUtil(Graph *g, int src, int dest, std::vector<bool>& visited,std::list<Node>& path){

    visited[src - 1] = true;
    SearchAlgo::totalVisited++;

    path.push_back(g->operator [](src));

    if(src == dest)
        return path;
    for (auto n : g->operator [](src).getDestinations()){
        if(n.getId() != -1)
            if(visited[n.getId() - 1] == false)
                return DFSUtil(g,n.getId(),dest,visited,path);
    }
    return path;
}

std::list<Node> SearchAlgo::DFSRecursive(Graph *g, int src, int dest){
    //create vector of bools to tell what nodes have been visited
    std::vector<bool> visited(g->getSize(),false);
    std::list<Node> path;
    DFSUtil(g,src,dest,visited,path);
    return path;
}

std::list<Node> SearchAlgo::DFSIterative(Graph *g, int src, int dest){
    std::vector<bool> visited(g->getSize(),false);
    std::vector<Node> frontier;
    std::list<Node> path;
    std::stack<Node> stack;


    stack.push((*g)[src]);
    while(!stack.empty()){
        Node temp = (*g)[stack.top().getId()];
        path.push_back(temp);
        visited[temp.getId() - 1] = true;
        SearchAlgo::totalVisited++;

        stack.pop();


        if(temp.getId() == dest)
            return path;

        frontier = ((*g).operator [](temp.getId())).getDestinations();
        std::reverse(frontier.begin(),frontier.end());

        for(auto n : frontier){
            if(n.getId() != -1)
                if(!visited[n.getId() -1])
                    stack.push(n);
        }
    }


    return path;
}

std::list<Node> SearchAlgo::BFSIterative(Graph *g, int src, int dest){
    std::vector<bool> visited(g->getSize(),false);
    std::vector<Node> frontier;
    std::list<Node> path;
    std::queue<Node> queue;



    queue.push((*g)[src]);



    while(!queue.empty()){
        Node temp = (*g)[queue.front().getId()];
        path.push_back(temp);
        visited[temp.getId() - 1] = true;
         SearchAlgo::totalVisited++;

        queue.pop();


        if(temp.getId() == dest){
            std::list<Node> tempList;
            Node tempNode = path.back();
            tempList.push_back(tempNode);
            Node backNode = tempList.back();

            path.pop_back();
            while(path.size() > 0){

                tempNode = path.back();
                for(auto n : backNode.getDestinations()){
                    if(tempNode.getId() == n.getId()){
                        tempList.push_back(tempNode);
                        break;
                    }
                }
                backNode = tempList.back();
                path.pop_back();

            }
            std::reverse(tempList.begin(),tempList.end());

            return tempList;
        }



        frontier = ((*g).operator [](temp.getId())).getDestinations();

        for(auto n : frontier){
            if(n.getId() != -1)
                if(!visited[n.getId() -1]){
                    queue.push(n);
                    visited[n.getId() - 1] = true;
                     SearchAlgo::totalVisited++;
                }
        }
    }


    std::list<Node> tempList;
    Node tempNode = path.back();
    tempList.push_back(tempNode);
    Node backNode = tempList.back();

    path.pop_back();
    while(path.size() > 0){

        tempNode = path.back();
        for(auto n : backNode.getDestinations()){
            if(tempNode.getId() == n.getId()){
                tempList.push_back(tempNode);
                break;
            }
        }
        backNode = tempList.back();
        path.pop_back();

    }
    std::reverse(tempList.begin(),tempList.end());



    return tempList;
}

std::list<Node> SearchAlgo::BFSRecursive(Graph *g, int src, int dest){
    //create vector of bools to tell what nodes have been visited
    std::vector<bool> visited(g->getSize(),false);
    std::list<Node> path;
    std::queue<Node> q;
    q.push(g->operator [](src));
    BFSUtil(g,src,dest,q,visited,path);

    std::list<Node> tempList;
    Node tempNode = path.back();
    tempList.push_back(tempNode);
    Node backNode = tempList.back();

    path.pop_back();
    while(path.size() > 0){

        tempNode = path.back();
        for(auto n : backNode.getDestinations()){
            if(tempNode.getId() == n.getId()){
                tempList.push_back(tempNode);
                break;
            }
        }
        backNode = tempList.back();
        path.pop_back();

    }
    std::reverse(tempList.begin(),tempList.end());
    return tempList;
}

std::list<Node> SearchAlgo::BFSUtil(Graph *g, int src, int dest, std::queue<Node> &q, std::vector<bool> &visited, std::list<Node> &path){
    //set visited to true for this node
    visited[src -1] = true;
     SearchAlgo::totalVisited++;

    //add node to the search path
    path.push_back(g->operator [](src));



    //if src is the dest then return path
    if(src == dest)
        return path;

    //for each dest node in src node add dest node to queue
    for(auto n : g->operator [](src).getDestinations()){
        if(n.getId() != -1) {// check if its an empty node in case of using adjMatrix
            if(visited[n.getId() -1] == false){
                //add destination node to the queue
                q.push(g->operator [](n.getId()));
                visited[n.getId() -1] = true;
            }
        }
    }
    q.pop();
    BFSUtil(g,q.front().getId(),dest,q,visited,path);


    return path;

}


void SearchAlgo::Dijkstra(Graph *g, int src, int dest){
    std::list<Node> path;
    Node searchTree(src,0.0);
    Node nd = (*g)[src];
    Node* tPtr;
    std::priority_queue<std::pair<float,int>,std::vector<std::pair<float,int> >, std::greater<std::pair<float,int> > > frontier;
    size_t size = (*g).getSize();
    std::vector<bool> visited(size,false);
    std::vector<int> dist(size,INF);

    //add source node to path
   // Node t(src,0.0);
   // searchTree.addToDest(t);
    frontier.push(std::make_pair(0,nd.getId()));
    tPtr = &searchTree;

    dist[src] = 0;


    while(frontier.top().second != dest){

        nd = (*g)[frontier.top().second];

      //  visited[nd.getId()] = true;
        frontier.pop();

        for(auto n : (*g)[nd.getId()].getDestinations()){

            int v = n.getId();
            int cost = n.getWeight();

            //  If there is shorter path to v through nd
            if (dist[v] > dist[nd.getId()] + cost)
            {
                // Updating distance of v
                dist[v] = dist[nd.getId()] + cost;
                frontier.push(std::make_pair(dist[v], v));
                Node temp(v,dist[v]);
                tPtr->addToDest(temp);

             }
        }

        findPtr((*g)[src],frontier.top().second,tPtr);

    }

    std::cout << "asdf" << std::endl;


}

void SearchAlgo::findPtr(Node n, int id, Node * ptr){
    Node found(-1);
    for(auto i : n.getDestinations()){
        if(i.getId() == id){
            found = i;

        }
    }
    if(found.getId() != -1){
        ptr = &found;
        return;
    }
    else{
        for(auto j : n.getDestinations()){
            findPtr(j,id,ptr);
        }
        return;
    }
}









