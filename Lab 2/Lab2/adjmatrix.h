#ifndef ADJMATRIX_H
#define ADJMATRIX_H
#include "graph.h"
#include <vector>


class AdjMatrix : public Graph
{
public:
    AdjMatrix();
    ~AdjMatrix();
    size_t getSize();
    void insertWeight(int,int,float);
    void insertNode(Node&);
    void setGraphSize(int);
private:
    std::vector<Node> aMatrix;

    Node& operator[](int);

};

#endif // ADJMATRIX_H
