#ifndef ADJLIST_H
#define ADJLIST_H
#include "graph.h"
#include <map>
#include "node.h"
#include <string>

class AdjList : public Graph
{
public:
    AdjList();
    ~AdjList();
    void insertNode(Node&);
    Node& operator[](int);
    size_t getSize();
    void insertWeight(int,int,float);
    void setGraphSize(int);

private:
   std::map<int,Node> alist;
   int graphSize;
};

#endif // ADJLIST_H
