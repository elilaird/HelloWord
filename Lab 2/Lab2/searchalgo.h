#ifndef SEARCHALGO_H
#define SEARCHALGO_H
#include "adjlist.h"
#include "adjmatrix.h"
#include "treenode.h"
#include <iostream>
#include <typeinfo>
#include <list>
#include <stack>
#include <queue>
# define INF 0x3f3f3f3f



class SearchAlgo
{
public:
    SearchAlgo();
    SearchAlgo(int);
    static std::list<Node> DFSUtil(Graph* g,int src, int dest,std::vector<bool>& visited,std::list<Node>& path);
    static std::list<Node> DFSRecursive(Graph* g,int src, int dest);
    static std::list<Node> DFSIterative(Graph* g,int src, int dest);
    static std::list<Node> BFSIterative(Graph* g,int src, int dest);
    static std::list<Node> BFSRecursive(Graph* g,int src, int dest);
    static std::list<Node> BFSUtil(Graph* g, int src, int dest,std::queue<Node>& q,std::vector<bool>& visited, std::list<Node>& path);
    static void Dijkstra(Graph* g,int src,int dest);
    static  void findPtr(Node n,int id,Node*);
    static int totalVisited;


private:
    Graph* activeGraph;

};

#endif // SEARCHALGO_H
