#ifndef SORT_H
#define SORT_H
#include "algorithm.h"
#include <vector>
#include <chrono>
#include <fstream>


class Sort : public Algorithm
{
public:
    Sort();
    virtual ~Sort();
    void Load(char*);
    void Execute();
    void Display();
    void Stats();
    void Select(int);
    void Save();
    void Configure();
private:

    //vector of function pointers and active algorithm function pointer
    static std::vector<void(*)(std::vector<int>&)> algorithms;
    void(*activeAlgo)(std::vector<int>&);

    //type definitions used to make code shorter for the timers
    typedef std::chrono::high_resolution_clock clock;

    int _selection; //sorting algorithm selection
    int _numRecords; //sorting algorithm selection

    //total duration taken for each algorithm
    std::vector<std::chrono::duration<double> > sortTimes;
    std::vector<int> dataSet;

};

#endif // SORT_H
