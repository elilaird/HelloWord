#include "adjmatrix.h"

AdjMatrix::AdjMatrix()
{

}

AdjMatrix::~AdjMatrix(){

}

void AdjMatrix::insertNode(Node& n){
    aMatrix[n.getId() - 1].setId(n.getId());

    for(auto dest : n.getDestinations()){
        aMatrix[n.getId()- 1].getDestinations()[dest.getId() - 1].setId(dest.getId());
    }

}

void AdjMatrix::insertWeight(int srcID, int dest, float wgt){
    aMatrix[srcID - 1].getDestinations()[dest - 1].setWeight(wgt);
}

Node& AdjMatrix::operator[](int x){
    return aMatrix[x - 1];
}

size_t AdjMatrix::getSize(){
    return aMatrix.size();
}

void AdjMatrix::setGraphSize(int size){

    aMatrix.resize(size);
    for(int i = 0; i < size; i++)
        for(int j = 0;j < size;j++)
            aMatrix[i].getDestinations().push_back(Node(-1));
}
