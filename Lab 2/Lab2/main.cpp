#include <iostream>
#include "algorithm.h"
#include "search.h"


int main(int argc , char* argv[])
{
    enum algo {DFSITERATIVE,DFSRECURSIVE,BFSITERATIVE,BFSRECURSIVE,ALAST};
    enum graph {ADJLIST,ADJMATRIX,GLAST};
    Search s;

    int src, dest;
    if(argc > 1){
        src = atoi(argv[1]);
        dest = atoi(argv[2]);
    }
    else {
        src = std::rand() % 16;
        dest = std::rand() % 16;

    }


     s.Load();
    for(int i = DFSITERATIVE;i != ALAST;i++){ //for each search algorithm
        for(int g = ADJLIST;g != GLAST;g++){ //and for each graph type
            s.Select(i,g);
            s.Execute(src,dest);
            s.Display();
            s.Stats();
        }
    }
    return 0;
}
