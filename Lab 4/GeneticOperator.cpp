
#include "GeneticOperator.h"


void GeneticOperator::_mutate(population_t &population,float mutatePercentile, int swapAmount) {
    int swapCount = 0;

    //using random library from c++11
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_real_distribution<float> probability_range(0.0,1.0);
    std::uniform_int_distribution<int> index_range(0,(int)(population[0].second.size()) - 1);


    //for each chromosome on a random probability swap two random indexes swapAmount amount of times
    for(auto& chromosome : population){
        //randomly choose a probability between 0.0 and 1.0
        float probability = probability_range(generator);

        //if probability isn't in percentile... don't mutate
        if(probability > mutatePercentile)
            continue;

        while(swapCount < swapAmount){
            int index1 = index_range(generator), index2 = index_range(generator);

            //swap genes in chromosome
            int temp = chromosome.second[index1];
            chromosome.second[index1] = chromosome.second[index2];
            chromosome.second[index2] = temp;

            swapCount++;
        }
    }

}

void GeneticOperator::_rouletteSelection(population_t &oldPopulation, population_t &newPopulation) {
    std::vector<float> probabilities(oldPopulation.size());   //stores each probability P for each chromosome
    float fitnessSum = 0;
    int selectionAmount =  (int)(oldPopulation.size() * .40) - 1, selectionCount = 0;

    //using random library from c++11
    std::random_device rd;
    std::default_random_engine generator(rd());


    //calculate sum of fitness scores
    for(auto gene : oldPopulation)
        fitnessSum += gene.first;


    //calculate probability P for each chromosome
    for(int i = 0;i < probabilities.size();i++)
        probabilities[i] = (oldPopulation[i].first / fitnessSum);       // P = Individual Fitness / fitnessSum
    auto itMax = std::max_element(probabilities.begin(),probabilities.end());
    auto itMin = std::min_element(probabilities.begin(),probabilities.end());

    float max = *itMax;
    float min = *itMin;

    std::uniform_real_distribution<float> distribution(min,max);

    //select chromosomes using roulette wheel strategy until selection amount is reached
    while(selectionCount < selectionAmount - 1){
        float randVal = distribution(generator);

        //adds chromosome where the random number is in range of its probability
        for(int j = 0; j < probabilities.size();j++){
            if(randVal <= probabilities[j]){
                newPopulation.push_back(oldPopulation[j]);
                break;
            }

        }
        selectionCount++;
    }
    int d = 123;
}

void GeneticOperator::_eliteSelection(population_t &oldPopulation, population_t &newPopulation) {
    int selectionAmount = (int)(oldPopulation.size() * .40) - 1;
    population_t tempPopulation = oldPopulation;

    //sorts population from least to greatest (where least == best fitness value) by first pair value (aka fitness value)
    std::sort(tempPopulation.begin(),tempPopulation.end());

    //add elite chromosomes until selection amount is reached
    for(int i = 0; i < selectionAmount; i ++)
        newPopulation.push_back(tempPopulation[i]);
}

void GeneticOperator::_orderedCrossover(population_t &oldPopulation, population_t &newPopulation) {

    //call crossover and pass in 30% for the percentage of the population to create
    _crossover(oldPopulation,newPopulation,.30);

}

void GeneticOperator::_halfCrossover(population_t &oldPopulation, population_t &newPopulation) {

    //call crossover and pass in 50% for the percentage of the population to create
    _crossover(oldPopulation,newPopulation,.50);
}

void GeneticOperator::_crossover(population_t &oldPopulation, population_t &newPopulation,float percent){
    int chromosomeLength = (int)(oldPopulation[0].second.size()),
            subsetLength = (int)(oldPopulation[0].second.size() * .30), //subset length is percent% of chromosome length
            startIndex = 0,                                             //starting index of subset
            crossoverAmount = (int)(oldPopulation.size() * .60),        //crossover amount is 60% of population length
            crossoverCount = 0;

    std::vector<int> child1(chromosomeLength), child2(chromosomeLength),
            parent1(chromosomeLength),parent2(chromosomeLength);

    //using random library from c++11
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> distribution(0,(int)oldPopulation.size() - 1);
    std::uniform_int_distribution<int> chromosome_range(0,chromosomeLength - subsetLength - 1);

    if(percent != .50)
        startIndex = chromosome_range(generator);


    //crossover randomly selected chromosomes and add them to the new population until the cross over amount is reached
    while(crossoverCount <= crossoverAmount){
        //randomly select parents
        parent1 = oldPopulation[distribution(generator)].second;
        parent2 = oldPopulation[distribution(generator)].second;
        std::vector<int> subset1, subset2;

        for(int i = 0,k = startIndex; i < subsetLength; k++,i++){
            subset1.push_back(parent1[k]);
            subset2.push_back(parent2[k]);
        }


        //performs crossover to generate first child
        for(int i = 0,j = 0; i < chromosomeLength; i++){
            if(startIndex <= i && i < startIndex + subsetLength)                       // if i is in subset index range
                child1[i] = parent1[i];                                                 // add gene from parent 1 to the child 1
            else if(find(subset1.begin(),subset1.end(),parent2[j]) == subset1.end()){   // else if gene isn't in subset
                child1[i] = parent2[j];                                                 //add gene from parent2 to the child 1
                j++;
            } else {                                                                    //if in subset, increment j until its not
                while(find(subset1.begin(),subset1.end(),parent2[j]) != subset1.end())
                    j++;
                child1[i] = parent2[j];
                j++;
            }
        }
        //performs crossover to generate second child
        for(int i = 0,j = 0; i < chromosomeLength; i++){
            if(startIndex <= i && i < startIndex + subsetLength)                       // if i is in subset index range
                child2[i] = parent2[i];                                                 // add gene from parent 2 to the child 2
            else if(find(subset2.begin(),subset2.end(),parent1[j]) == subset2.end()){   // else if gene isn't in subset
                child2[i] = parent1[j];                                                 //add gene from parent 1 to the child 2
                j++;
            } else{
                while(find(subset2.begin(),subset2.end(),parent1[j]) != subset2.end())
                    j++;
                child2[i] = parent1[j];
                j++;
            }
        }

        //add children to the population
        newPopulation.push_back(std::make_pair(0.0,child1));
        crossoverCount += 1;
        if(crossoverCount != crossoverAmount){
            newPopulation.push_back(std::make_pair(0.0,child2));
            crossoverCount += 1;
        }


    }
}