

#ifndef LAB_3_GRAPH_H
#define LAB_3_GRAPH_H

#include <map>


class Graph {

public:
    std::map<int,std::tuple<float,float,float>> adjList;
    std::tuple<float,float,float> operator[](int);
    size_t size();

};


#endif //LAB_3_GRAPH_H
