
#include "TabuOperators.h"

TabuOperators::neighborhood_t TabuOperators::_neighborhoodCrossover(TabuOperators::solution_t parent,int size) {
    int startIndex,
        solLength = (int)(parent.second.size()),
        subsetLength = (int)(solLength * .60);

    neighborhood_t newNeighborhood;

    std::vector<int> origParent = parent.second;

    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> index_range(0, solLength - subsetLength - 1);

    startIndex = index_range(generator);

    //add the parent to its neighborhood
    newNeighborhood.push_back(parent);

    while(newNeighborhood.size() < size){
        //randParent to cross with is a random permutation of the original parent
        std::vector<int> randParent = parent.second;
        random_shuffle(randParent.begin(),randParent.end());

        std::vector<int> child(solLength);

        //subsets used to crossover
        std::vector<int> subset1;

        //adding values from parents to subsets
        for(int i = 0,j = startIndex; i < subsetLength; j++,i++)
            subset1.push_back(origParent[j]);

        //performs crossover to generate first child
        for(int i = 0,j = 0; i < solLength; i++){
            if(startIndex <= i && i < startIndex + subsetLength)                            // if i is in subset index range
                child[i] = origParent[i];                                                   // add node from origParent to the child
            else if(find(subset1.begin(),subset1.end(),randParent[j]) == subset1.end()){     // else if node isn't in subset
                child[i] = randParent[j];                                                   //add gene from parent2 to the child
                j++;
            } else {                                                                        //if in subset, increment j until its not
                while(find(subset1.begin(),subset1.end(),randParent[j]) != subset1.end())
                    j++;
                child[i] = randParent[j];
                j++;
            }
        }

        newNeighborhood.push_back(std::make_pair(0.0,child));
    }

    return newNeighborhood;



}

TabuOperators::neighborhood_t TabuOperators::_neighborhoodSwap(TabuOperators::solution_t parent, int size) {
    int swapAmount = 1,//(int)(parent.second.size() * .2),
        swapcount = 0;
    neighborhood_t newNeighborhood;

    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> index_range(0,(int)(parent.second.size()) - 1);

    //add parent to its neighborhood
    newNeighborhood.push_back(parent);
    int i = 0;
    //until size is reached, make new solutions by looping through indexes of solution and swapping them with a random index
    while(newNeighborhood.size() < size){
        //set newSol to initially be a copy of the original parent
        std::vector<int> newSol = parent.second;
        if(i == newSol.size())
            i = 0;

        //swap
        int index2 = index_range(generator);
        int temp = newSol[i];
        newSol[i] = newSol[index2];
        newSol[index2] = temp;

        i++;

        //add new solution to the neighborhood
        newNeighborhood.push_back(std::make_pair(0.0,newSol));
    }

    return newNeighborhood;
}