
#ifndef LAB_3_TABUSEARCH_H
#define LAB_3_TABUSEARCH_H

#include "Algorithm.h"
#include "Graph.h"
#include "TabuOperators.h"
#include <vector>
#include <string>
#include <chrono>
#include <iostream>
#include <sstream>
#include <random>
#include <map>

class TabuSearch : public Algorithm{

//public type definitions for solution and high_resolution_clock
public:
    typedef std::pair<float,std::vector<int>> solution_t;
    typedef std::vector<solution_t> neighborhood_t;
    typedef std::chrono::high_resolution_clock clock;

    //neighborhood selection type enums
    enum selectionTypes {
        CROSSOVER = 0,
        SWAP
    };

    const std::string neighborhoodStrings[2] = {"Crossover Neighborhood Generation", "Swap Neighborhood Generation"};

//private attributes
private:

    //graph used to access nodes and weights
    Graph* searchGraph;

    //vector of each node in the graph
    std::vector<int> nodes;

    //final solution
    std::pair<float,std::string> finalSolution;

    //current selected neighborhood generation type
    std::string generationType;

    //array of correct solutions found from running dynamic tsp
    std::string dynamicSolutions[13] = {"","","","","1 5 2 3 4 1","1 5 2 3 4 6 1","1 6 4 3 2 5 7 1","1 6 4 3 2 8 5 7 1","1 6 9 4 3 2 8 5 7 1","1 6 9 4 10 3 2 8 5 7 1","1 6 9 4 11 10 3 2 8 5 7 1","1 6 9 4 11 10 3 12 2 8 5 7 1", "1 6 9 4 11 10 3 12 2 8 5 13 7 1"};
    float dynamicCosts[13] = {0.0,0.0,0.0,0.0,62.8194,73.8208,105.911,118.847,124.228,143.522,153.896,182.424,196.573};

    float dynamicCost;
    //current dynamic solution
    std::string dynamicSolution;
    std::string reversedSolution;

    //dynamic execution time (used to compare)
    std::chrono::duration<int,std::micro> dynamicTime;

    //execution time
    std::chrono::microseconds executionTime;

    //total iterations
    int totalIterations;

    //function pointers for both neighborhood selection techniques
    neighborhood_t(*neighborhoodSelector)(solution_t,int);

    //map of function pointers for neighborhood selection
    std::map<TabuSearch::selectionTypes,neighborhood_t(*)(solution_t,int)> neighborhoodSelections;

    //Tabu List
    std::vector<std::string> tabuList;

    //Tabu List size
    size_t tabu_size;


public:
    TabuSearch();
    TabuSearch(Graph*);
    void load();
    void select(Algorithm::algoType);
    void select(selectionTypes selectionType, size_t listSize);
    void execute();
    void output();
    void display();

private:

    std::pair<float,std::string> tabu_search();
    solution_t generateInitialSolution();
    solution_t calculateBest(neighborhood_t&);
    void exitLocalMin(solution_t&);


    //Tabu List Functions
    bool checkTabu(solution_t);
    void addToTabu(solution_t);

    //Utility Functions
    std::string solutionToString(solution_t);
    float calculateDistance(std::tuple<float, float, float>, std::tuple<float, float, float>);

};


#endif //LAB_3_TABUSEARCH_H
