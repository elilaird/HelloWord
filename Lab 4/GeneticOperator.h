#ifndef LAB_3_GENETICOPERATOR_H
#define LAB_3_GENETICOPERATOR_H

#include <vector>
#include <string>
#include <cmath>
#include <random>
#include <algorithm>

class GeneticOperator {

public:
    typedef std::vector<std::pair<float,std::vector<int>>> population_t;

    static void _mutate(population_t&,float percentage,int swapAmount);
    static void _rouletteSelection(population_t&,population_t&);
    static void _eliteSelection(population_t&,population_t&);
    static void _orderedCrossover(population_t&,population_t&);
    static void _halfCrossover(population_t&,population_t&);

private:
    static void _crossover(population_t&,population_t&,float);



};


#endif //LAB_3_GENETICOPERATOR_H
