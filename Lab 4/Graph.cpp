

#include "Graph.h"

std::tuple<float,float,float> Graph::operator[](int i) {
    return adjList.at(i);
}

size_t Graph::size() {
    return adjList.size();
}