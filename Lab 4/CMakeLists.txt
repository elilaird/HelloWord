cmake_minimum_required(VERSION 3.12)
project(Lab_3)

set(CMAKE_CXX_STANDARD 14)

add_executable(Lab_3 main.cpp Algorithm.h HamiltonianCircuit.cpp HamiltonianCircuit.h AlgoAnalyst.cpp AlgoAnalyst.h TravelingSalesman.cpp TravelingSalesman.h Graph.cpp Graph.h FileHandler.cpp FileHandler.h GeneticAlgorithm.cpp GeneticAlgorithm.h GeneticOperator.cpp GeneticOperator.h TabuSearch.cpp TabuSearch.h TabuOperators.cpp TabuOperators.h)