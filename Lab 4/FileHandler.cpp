

#include "FileHandler.h"



std::map<int,std::tuple<float ,float ,float >> FileHandler::parse() {
    std::ifstream iFile("positions.txt");
    size_t comma;
    std::map<int,std::tuple<float,float,float>> adjList;

    for(int i = 0; i < 11;i++) {

        int id;
        float pos1, pos2, pos3;
        std::string temp;
        iFile >> temp;
        std::stringstream ss;

        //remove commas
        comma = temp.find(',');
        while (comma != std::string::npos) {
            temp.replace(comma, 1, " ");
            comma = temp.find(',');
        }

        ss << temp;
        ss >> id;
        ss >> pos1;
        ss >> pos2;
        ss >> pos3;

        //insert into adjList
        adjList.insert(std::make_pair(id, std::make_tuple(pos1, pos2, pos3)));
    }
    iFile.close();

    return adjList;
}