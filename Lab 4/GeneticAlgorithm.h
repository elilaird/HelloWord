
#ifndef LAB_3_GENETICALGORITHM_H
#define LAB_3_GENETICALGORITHM_H

#include "Algorithm.h"
#include "Graph.h"
#include "GeneticOperator.h"
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <chrono>

#define INF 0x3f3f3f3f

class GeneticAlgorithm  : public Algorithm {

//public type definition for population, chromosome, and high_resolution_clock
public:
    typedef std::vector<std::pair<float,std::vector<int>>> population_t;
    typedef std::pair<float,std::vector<int>> chromosome_t;
    typedef std::chrono::high_resolution_clock clock;

//selection and crossover type enums
    enum selectionTypes {
        ROULETTE = 0,
        ELITE
    };

    enum crossoverTypes {
        ORDERED = 0,
        HALFNHALF
    };

    const std::string selectionStrings[2] = {"Roulette Wheel Selection", "Elite Selection"};
    const std::string crossoverStrings[2] = {"Ordered Crossover", "Half and Hald Crossover"};


//private attributes
private:

    //array of correct solutions found from running dynamic tsp
    std::string dynamicSolutions[12] = {"","","","","1 5 2 3 4 1","1 5 2 3 4 6 1","1 6 4 3 2 5 7 1","1 6 4 3 2 8 5 7 1","1 6 9 4 3 2 8 5 7 1","1 6 9 4 10 3 2 8 5 7 1","1 6 9 4 11 10 3 2 8 5 7 1","1 6 9 4 11 10 3 12 2 8 5 7 1"};

    //current dynamic solution
    std::string dynamicSolution;
    std::string reversedSolution;

    //graph object pointer stores possible nodes and their positions
    Graph* searchGraph;

    //list contains each node in the graph
    std::vector<int> nodes;

    //pair that contains the final solution
    std::pair<float,std::string> solution;

    //total amounts of epocs surpassed before algorithm was terminated
    int totalEpocs;

    //high_resolution_clock instance used to store the total execution time
    std::chrono::microseconds executionTime;


    //dynamic execution time
    std::chrono::duration<int,std::micro> dynamicTime;

    //strings for selection and crossover types
    std::string selectionStr, crossoverStr;

    //function pointers for selection, crossover, and mutate functions
    void(*activeSelection)(population_t&,population_t&);
    void(*activeCrossOver)(population_t&,population_t&);
    void(*mutate)(population_t&,float,int);

    //maps of each type of function pointer (selection and crossover)
    std::map<GeneticAlgorithm::selectionTypes,void(*)(population_t&,population_t&)> selections;
    std::map<GeneticAlgorithm::crossoverTypes, void(*)(population_t&,population_t&)> crossovers;

    //mutation amount and percentage
    int mutationAmount;
    float mutationPercent;

//public function definitions
public:
    GeneticAlgorithm();
    GeneticAlgorithm(Graph*);
    void load();
    void select(Algorithm::algoType);
    void select(selectionTypes selectionType,crossoverTypes crossoverType,float percent,int amount = 1);
    void execute();
    void output();
    void display();

//private function definitions
private:

    //utility functions
    population_t  generateInitialPopulation();
    chromosome_t calculateFitness(population_t&);
    float calculateDistance(std::tuple<float, float, float> n1, std::tuple<float, float, float> n2);
    std::pair<float,std::string> genetic_algorithm();

};


#endif //LAB_3_GENETICALGORITHM_H
