
#include "AlgoAnalyst.h"
#include "HamiltonianCircuit.h"
#include "GeneticAlgorithm.h"
#include "TabuSearch.h"
#include <iostream>
#include <random>

int main(int argc, const char * argv[]) {


    AlgoAnalyst analyst;
    analyst.load();

//    HamiltonianCircuit* tsp = (HamiltonianCircuit*)(analyst.generateAlgorithm(AlgoAnalyst::HAMILTONIANCIRCUIT));
//    tsp->select(Algorithm::TSPDYNAMIC);

    GeneticAlgorithm* tsp = (GeneticAlgorithm*)(analyst.generateAlgorithm(AlgoAnalyst::GENETICALGORITHM));
    tsp->select(GeneticAlgorithm::ROULETTE,GeneticAlgorithm::ORDERED,5.0);

//    TabuSearch * tsp = (TabuSearch*)(analyst.generateAlgorithm(AlgoAnalyst::TABUSEARCH));
//    tsp->select(TabuSearch::SWAP,10);

    tsp->execute();
    tsp->display();

//    tsp->select(TabuSearch::CROSSOVER,10);
//    tsp->execute();
//    tsp->display();



    return 0;
}
