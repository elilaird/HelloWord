
#include "TravelingSalesman.h"



//******************* Public Wrapper Functions ********************************

std::pair<float,std::string> TravelingSalesman::tfs_naive(Graph* graph) {
    std::vector<bool> visited(graph->size() + 1,false);
    std::vector<std::vector<int>> paths;
    std::stack<int> path;
    int callCount = 0;

    //number of iterations is the factorial of the size of the graph - 1 divided by 2
    int numIter = factorial(graph->size() - 1) / 2;

    tfs_naiveUtil(graph,visited,paths,path,1,graph->size(),++callCount,numIter);

    //calculate the shortest path by passing paths into calcShortest
    std::pair<float,std::vector<int>> shortestPath = calcShortest(graph, paths);

    std::string temp = "";
    for(int i = 0;i < shortestPath.second.size();i++){
        temp += std::to_string(shortestPath.second[i]) + ">";
    }
    temp += "1";

    return std::make_pair(shortestPath.first,temp);
}

std::pair<float,std::string> TravelingSalesman::tfs_dynamic(Graph* graph) {

    int numCalls = 0;
    std::vector<int> frontier;
    std::pair<float,std::string> pathCost;
    std::vector<std::vector<float>> cache(graph->size() + 1,std::vector<float>(graph->size() + 1,0));

    //store all nodes except the starting node on the frontier vector
    for(auto it = graph->adjList.begin(); it != graph->adjList.end();it++){
        if(it->first != 1)
            frontier.push_back(it->first);
    }

    //fill in the cache with distances beforehand
    int k = 1;
    for(int i = 1; i <= graph->size();i++){

        for(int j = k; j <= graph->size();j++){
            cache[i][j] = calcDistance(graph->operator[](i),graph->operator[](j));
            cache[j][i] = cache[i][j];
        }
        k++;
    }

    pathCost = tfs_dynamicUtil(graph,frontier,1,cache,++numCalls);

    std::string temp = pathCost.second;
    for(int i = 0;i < temp.length();i++){
        if(temp[i] == '_')
            temp[i] = '>';
    }


    return std::make_pair(pathCost.first,temp);
}





//****************** Private Utility Functions ***********************************

//implements a naive approach to the traveling salesman problem
void TravelingSalesman::tfs_naiveUtil(Graph* graph, std::vector<bool> visited,std::vector<std::vector<int>> &paths, std::stack<int> &path, int current, int numCount, int &callCount, int &numIter) {

    callCount++;
    visited[current] = true;
    path.push(current);
    //if the current path has reached the path size, store path
    if(path.size() == numCount){
        paths.push_back(storePath(paths,path));
    }


    //loop through children of the current node and recurse
    for(int i = 1; i <= numCount;i++){

        if(!visited[i]){
            tfs_naiveUtil(graph,visited,paths,path,i,numCount,++callCount,numIter);
        }

    }

    //pop top from the stack
    path.pop();
    return;
}

//implements a dynamic approach to the traveling salesman problem
std::pair<float,std::string> TravelingSalesman::tfs_dynamicUtil(Graph* graph, std::vector<int> relFrontier, int src, std::vector<std::vector<float>> &cache, int &numCalls) {
    float relMin = INF;
    std::string minNode = "";
    std::pair<float,std::string> tempPair;

    //check for base case .. where frontier size equals 0
    if(relFrontier.size() == 0){

        tempPair = std::make_pair(cache[src][1],("_" + std::to_string(src) + "_" + "1"));
        return tempPair;
    }


    //loop through nodes on frontier and recursive call the subproblems
    for(int i = 0; i < relFrontier.size();i++){
        int temp = relFrontier[i];
        float tempCost;
        std::vector<int> tempFrontier = relFrontier;

        tempFrontier.erase(tempFrontier.begin() + i);
        tempPair = tfs_dynamicUtil(graph,tempFrontier,temp,cache,++numCalls);

        tempCost = cache[src][temp] + tempPair.first;

        //check if min path
        if(tempCost < relMin){
            relMin = tempCost;
            minNode = "_" + std::to_string(src) + tempPair.second;
        }
    }

    return std::make_pair(relMin,minNode);
}



//return factorial of num
int TravelingSalesman::factorial(int num) {
    if(num == 1)
        return 1;
    else
        return num * factorial(num - 1);
}

//calculate the distance between two points
float TravelingSalesman::calcDistance(std::tuple<float, float, float> n1, std::tuple<float, float, float> n2) {
    float distance = 0.0;
    float x1 = std::get<0>((n1)) , y1 = std::get<1>((n1)) , z1 = std::get<2>((n1));
    float x2 = std::get<0>((n2)) , y2 = std::get<1>((n2)) , z2 = std::get<2>((n2));

    //use distance formula to calculate distance
    distance  += sqrt((pow((x1 - x2),2.0)) + (pow((y1 - y2),2.0)) + (pow((z1 - z2),2.0)));

    return distance;
}

//calculate the shortest path in the vector of paths
std::pair<float,std::vector<int>> TravelingSalesman::calcShortest(Graph* graph, std::vector<std::vector<int>> paths) {
    float min = INF;
    float pathTotal = 0.0;
    std::vector<int> shortestPath;

    //loop through each path and calculate distance traveled
    for(auto  s : paths){
        pathTotal = 0.0;
        for(int i = 0; i < s.size() - 1;i++){
            std:: tuple<float,float,float> n1 = graph->operator[](s[i]), n2 = graph->operator[](s[i + 1]);
            pathTotal += calcDistance(n1,n2);

        }
        pathTotal += calcDistance(graph->operator[](s[s.size() - 1]),graph->operator[](1));
        if(pathTotal < min){
            min = pathTotal;
            shortestPath = s;
        }
    }

    return std::make_pair(min,shortestPath);
}

//store the path from the stack into the vector of paths
std::vector<int> TravelingSalesman::storePath(std::vector<std::vector<int>> &paths, std::stack<int> path) {

    //pops the path from the stack and stores it into a string
    std::vector<int> temp;
    while(!path.empty()){
        temp.push_back(path.top());
        path.pop();
    }
    std::reverse(temp.begin(),temp.end());
    return temp;



}
